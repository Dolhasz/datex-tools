This is a simple prototype tool for dealing with the Datex II standard. The aim
is to develop a Datex II toolchain, enabling 3rd party developers to interact
with this standard without the need for researching and understanding the entire
Datex II standard. 

Currently, this works with Oxfordshire county traffic data (see API call) and 
only deals with a few types of this data. This is currently not production-ready,
but provides a template to how this data could be dealt with.

In order to test run:

```python datex.py```

You should see two plots showing the distribution of latest flows and speeds 
from the Oxfordshire UTMC API.

Currently only supports JSON (XML support may be added in the future).