#!python3
import datetime
import json
import matplotlib.pyplot as plt
import numpy as np
import requests
import argparse

# These are currently supported traffic classes
TRAFFIC_CLASSES = {
    'flow': 'vehicleFlowField',
    'speed': 'averageVehicleSpeedField'
}

# These are currently supported data classes, and their mappings to relevant fields in json
# see http://d2docs.ndwcloud.nu/_static/data/v2.3/DATEXII-UserGuide.pdf chapter 4 for details
DATA_CLASSES = {
    'PredefinedLocationsPublication' : 'predefinedLocationsField',
    'ElaboratedDataPublication' : 'elaboratedDataField'
}

# Need to start subclassing - just a case of following the Datex standard

# top-level class:
#   check_datex_type()
#   instantiate_relevant_subclass
#   return instance

class Datex:
    def __init__(self, json_string):
        # Quick way to load from file by passing path
        if json_string.endswith('.json'):
            with open(json_string, 'r', encoding="utf8") as fp:
                lines = ''.join(fp.readlines())
                raw_data = json.loads(lines)

        # Otherwise assume string
        else:
            raw_data = json.loads(json_string)
        self.raw_data = self._check_type(raw_data)
        publication = self.raw_data['d']['payloadPublicationField']

        # Check if datex is static locations or data
        try:
            self.datex_type = DATA_CLASSES[publication['__type'].split(':')[0]]
        except Exception as error:
            raise Exception('Sorry, we dont support this datex data type yet, feel free to build this functionality in yourself') from error


        # Get payload
        self.payload = publication[self.datex_type]

    def _check_type(self, raw_data):
        assert raw_data['d'], 'No data present in json'
        assert raw_data['d']['payloadPublicationField'], 'No payload present in Datex file'
        return raw_data

    def _get_traffic_feature(self, feature):
        feature = feature.lower()
        output = []
        for item in self.payload:
            if item is not None and item['basicDataValueField']:
                if feature in item['basicDataValueField']['__type'].lower():
                    value = float(item['basicDataValueField'][TRAFFIC_CLASSES[feature]])
                    scn = item['basicDataValueField']['affectedLocationField']['locationContainedInGroupField'][0]['predefinedLocationReferenceField']
                    ts = ep2ts(item['basicDataValueField']['timeField']['valueField'])
                    output.append((scn, ts, value))

        return output

    def get_flows(self):
        return self._get_traffic_feature('flow')

    def get_speeds(self):
        return self._get_traffic_feature('speed')

    def get_locations(self):
        pass

def get_data(url):
    r = requests.get(url, auth=('OCC_Temp_User1', 'Md7s53HvSj'))
    return r.text

def ep2ts(epoch):
    ts_raw = [ch for ch in epoch if ch.isdigit() or ch == '+']
    ts_raw = ts_raw[:10]
    ts_raw = ''.join(ts_raw)

    return datetime.datetime.utcfromtimestamp(float(ts_raw))

def run_tests(api_response):
    datex_file = Datex(api_response)
    speeds = datex_file.get_speeds()
    flows = datex_file.get_flows()

    today_data = [x for x in speeds if x[1] >= datetime.datetime.today().replace(hour=0, minute=0, second=0)]
    scns = [d[0] for d in today_data]
    speeds = [d[2] for d in today_data]

    print('{} sensors outputting speed data'.format(len(np.unique(scns))))
    [print(s) for s in scns]

    today_data = [x for x in flows if x[1] >= datetime.datetime.today().replace(hour=0, minute=0, second=0)]
    scns = [d[0] for d in today_data]
    flows = [d[2] for d in today_data]

    print('{} sensors outputting flow data'.format(len(np.unique(scns))))
    [print(s) for s in scns]

    plt.figure()
    plt.title('Speed distribution')
    plt.hist(speeds, 20)
    plt.show()
    
    plt.figure()
    plt.title('Flow distribution')
    plt.hist(flows, 20)
    plt.show()


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--url')
    args = parser.parse_args()

    if not args.url:
        url = "http://datexii.oxfordshirevoyager.com/CloudAmber/OxfordshireDateXIIJson/Json/GetDateXIITrafficData"
    else:
        url = args.url

    response = get_data(url)

    run_tests(response)
