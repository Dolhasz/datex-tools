#!python3
import json
import xml.etree.cElementTree as ET
# try:
#     from lxml import etree as ET
# except ImportError:
#     import xml.etree.cElementTree as ET
#     print('For higher speed, pip install lxml')


# These are currently supported traffic classes
TRAFFIC_CLASSES = {
    'flow': 'vehicleFlowField',
    'speed': 'averageVehicleSpeedField'
}

# These are currently supported data classes, and their mappings to relevant fields in json
# see http://d2docs.ndwcloud.nu/_static/data/v2.3/DATEXII-UserGuide.pdf chapter 4 for details
DATA_CLASSES = {
    'PredefinedLocationsPublication': 'predefinedLocationsField',
    'ElaboratedDataPublication': 'elaboratedDataField'
}

# Helper functions for checking ElementTree elements

def hasText(element):
    '''
    Checks if element contains text
    '''
    return element.text is not None

def hasAttrib(element):
    '''
    Checks if element contains attributes
    '''
    return len(element.attrib) > 0

def hasElement(element):
    '''
    Checks if element contains additional elements
    '''
    return len(list(element)) > 0

def getTag(payload, tag):
    '''
    Takes Element and string, returns full tag name containing given string
    '''
    return [x.tag for x in payload if tag in x.tag][0]


class ElaboratedData():
    PROPS = (
        'averageVehicleSpeed',
        'travelTime',
        'normallyExpectedTravelTime',
        'pertinentLocation',
        'measurementOrCalculationTime',
        'trafficSpeedExtension',
        'freeFlowTravelTime'
    )

    def __init__(self, payload):
        # Printing stuff for debug purposes
        print('__New Class Instance__', end='\n\n')

        # Check for 'forecast' flag
        if any(['forecast' in item.tag.lower() for item in payload]):
            self.forecast = True

        # Check if basicData is present
        try:
            data = payload.find(getTag(payload, 'basicData'))
        except:
            raise Exception('No basicData field present.')

        # Grab the type of data
        self.type = data.items()[0][1]

        # Create property for each item in payload
        [self.handle_property(data, item.tag) for item in data]

        # Grab relevant data and remove intermediate gunk (this essentially throws out some data)
        self.unpack_properties()

        # DEBUG: print dynamically generated properties and their values
        for p in self.__dict__:
            if p == 'data':
                continue
            prop = getattr(self, p)
            print('{}:{}'.format(p, prop))

    def to_json(self):
        '''
        Convert class to json
        '''
        jason = {}
        for prop in self.__dict__:
            if prop != 'data':
                jason[prop] = getattr(self, prop)

        return json.dumps(jason, indent=4)

    def handle_property(self, data, tag):
        '''
        Create property from each item in payload, if it exists in allowed properties.
        If a disallowed property is encountered, raise an alarm
        '''
        for prop in self.PROPS:
            if prop in tag:
                setattr(self, prop, data.find(tag))
                return

        raise ValueError('Property {} not in {}'.format(prop, self.PROPS))

    def unpack_properties(self):
        '''
        Lazily check if properties have additional elements or subelements.
        This should be recursive, but it seems there's never more than a
        couple of levels to drill down into in the ElaboratedData class.
        '''
        for d in self.__dict__:
            if d in ('data', 'type', 'forecast'):
                continue
            prop = getattr(self, d)

            if hasAttrib(prop) and hasElement(prop):
                # This has both an attribute (most likely a type) and additional elements
                for element in list(prop):
                    if hasAttrib(element) and hasElement(element):
                        # Currently not happening
                        pass
                    elif hasAttrib(element) and not hasElement(element):
                        # THIS IS LIKELY A LOCATION
                        setattr(self, d, element.get('id'))

            elif hasText(prop) and not hasAttrib(prop) and not hasElement(prop):
                # This is a bare data item, just has a text field
                setattr(self, d, prop.text)

            elif hasElement(prop) and not hasAttrib(prop) and prop[0].text is not None:
                # This is an element with just a subelement that contains a value
                setattr(self, d, float(prop[0].text))

            elif hasElement(prop) and not hasAttrib(prop) and prop[0].text is None:
                # This is an element with a subelement and another one within (which may have text)
                setattr(self, d, prop[0][0].text)

            elif hasAttrib(prop) and not hasElement(prop):
                Exception('Attrib but no element')

            elif not hasAttrib(prop) and hasElement(prop):
                Exception('Element but no attrib')


class Datex_xml():
    def __init__(self, xml):
        xml = bytes(bytearray(xml, encoding='utf-8'))
        e = ET.fromstring(xml)

        if 'd2LogicalModel' not in e.tag:
            raise Exception("This doesn't look like proper Datex II xml")

        if not all(['exchange' or 'payloadPublication' in item for item in e]):
            raise Exception("This doesn't look like proper Datex II xml")

        self.log_info(e)
        self.contents = self.parse_publication_types(e)

    def parse_publication_types(self, e):
        exchange, payloadPublication = e
        contents = []

        for item in payloadPublication:
            if 'elaboratedData' in item.tag:
                eD = ElaboratedData(item)
                print('As json:')
                print(eD.to_json(), end='\n\n')
                contents.append(eD)

    def log_info(self, e):
        exchange, payloadPublication = e
        last = ''
        counts = {}
        for item in payloadPublication:
            payloadType = item.tag.split('}')[-1]
            if payloadType not in last:
                last = payloadType
                counts[payloadType] = 1
            else:
                counts[payloadType] += 1


if __name__ == '__main__':

    import zipfile
    import time
    # import xml.dom.minidom

    start_time = time.time()
    with zipfile.ZipFile('sample_data/sample_data.zip', 'r') as myzip:
        for n in myzip.namelist():

            d = myzip.read(n).decode('utf-8')
            x = Datex_xml(d)

            # Dirty prettyprint
            # if n == 'sensors1.xml':
            #     dom = xml.dom.minidom.parseString(data)
            #     pretty_xml_as_string = dom.toprettyxml()
            #     print(pretty_xml_as_string)

    with open('./sample_data/glasgow.xml', 'rb') as fp:
        Datex_xml(fp.read().decode('utf-8'))

    print(time.time() - start_time)
