import requests
import datetime
import json


TOKEN_URL="https://Vdanet-beta.ca-traffic.com/Token"
LIST_URL="https://vdanet-beta.ca-traffic.com/api/SiteList/VDAPro-TransportForWestMids/All/NULL/1970-1-1/1970-1-1"
COUNTER_URL="https://vdanet-beta.ca-traffic.com/api/FlowData/VDAPro-TransportForWestMids/"
HEADER={'content-type':'application/x-www-form-urlencoded'}

class CycleSite:
	def __init__(self, obj):
		self.site_ref = str(obj['SiteReference'])
		self.id = obj['SiteID']
		self.num = obj['SiteNumber']
		self.lat = obj['SiteLatitude']
		self.lon = obj['SiteLongitude']
		self.desc = obj['SiteTitle']
		if any([x == None for x in self.__dict__.values()]):
			self.complete = False
		else:
			self.complete = True

	def __str__(self):
		return"""
		Cycle Site #{id} \n
		Description: {desc} \n
		Lat: {lat} \n
		Lon: {lon} \n
		""" .format(
			id=self.site_ref,
			desc=self.desc,
			lat=self.lat,
			lon=self.lon
		)

	def __repr__(self):
		return self.__str__()


class CycleChannel:
	def __init__(self, obj):
		self.direction = obj['ChannelName'] if obj['ChannelName'] in ('Northbound', 'Southbound', 'Eastbound', 'Westbound') else 'Unknown'
		self.number = obj['ChannelNo']
		self.in_total = bool(obj['InTotal'])
		self.key_flow = int(obj['KeyFlow'])
		flows = obj['Flows'][0]
		self.date = datetime.datetime.strptime(flows['DateOfData'], '%Y-%m-%dT%H:%M:%S').date()
		self.data = {int(int(k)/60) : v for k, v in flows['Values'].items()}

	def __str__(self):
		return"""Channel #{n}:{name} \n Data for {dt}: \n {p} \n""" .format(
			name=self.direction,
			n=self.number,
			dt=self.date,
			p=self.data
		)

	def __repr__(self):
		return self.__str__()


class CycleData:
	def __init__(self, obj):
		if 'properties' in obj.keys():
			self.error = bool(obj['properties']['ReportError']['HasError'])
		if not self.error:
			self.flow = [CycleChannel(ch) for ch in obj['properties']['Channels']]


class CycleApi:
	def __init__(self, auth_payload, url=TOKEN_URL, header=HEADER):
		# Get token
		auth = requests.post(url, data=auth_payload, headers=HEADER).json()
		self.auth={'Authorization':'Bearer {}'.format(auth['access_token'])}
		# Load site list
		self.sites=self.get_sites()
		
	def get_sites(self, url=LIST_URL):
		x = requests.get(url, headers=self.auth).json()
		res = [CycleSite(obj) for obj in x['properties']]
		return res
		
	def get_data(self, site, startdate, enddate, bins=60, url=COUNTER_URL):
		assert (enddate-startdate).days < 31, "Query period is greater than a month"

		start = datetime.datetime.strftime(startdate, "%Y-%m-%d")
		end = datetime.datetime.strftime(enddate, "%Y-%m-%d")
		query = '{}/{}/{}/{}'.format(site.id, start, end, bins)
		
		x = requests.get(url+query, headers=self.auth).json()

		return CycleData(x)
		
		
		
		



