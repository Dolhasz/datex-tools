import requests
import datetime
import json
import cycleapi

with open('config.json',"r") as j:
	payload=json.load(j)

x=cycleapi.CycleCounters(payload)

# print json.dumps(sorted([[n,x.sitelist[n]['title']] for n in x.sitelist]),indent=2)

graph="EX01A"
teststart="27/02/2019"
testend="28/02/2019"
dtstart=datetime.datetime.strptime(teststart,"%d/%m/%Y")
dtend=datetime.datetime.strptime(testend,"%d/%m/%Y")
res=x.getdata(graph,dtstart,dtend, bins=15)
'''
count=0
sites=[]
for graph in sorted(x.sitelist):
	try:
		res=x.getdata(graph,dtstart,dtend)
		print graph,len(res)
		if len(res)>0:
			count+=1
		sites.append(graph)
	except:
		print graph,'other error'
print count
print sites
'''
#print sorted(res[(1,'Southbound')])
import matplotlib.pyplot as plt
w=1
for n in sorted(res):
	#print [a[0].hour+w for a in res[n]]
	plt.bar([a[0].hour+w for a in res[n]],[a[1] for a in res[n]],align='center',width=0.25,label=n)
	w+=0.25
plt.gca().legend()
plt.title(x.sitelist[graph]['title']+str(x.sitelist[graph]['location']))
plt.show()



