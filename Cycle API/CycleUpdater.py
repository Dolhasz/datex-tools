import cycleapi as api
import psycopg2
from psycopg2.extras import execute_batch
import json
from datetime import datetime, timedelta

with open('db.json', 'r') as f:
    dbauth = json.load(f)

# class BirtCycleSample:
#     def __init__(self, obj):
#         self.props = 'sensor_id flow direction missing update_time'
#         [setattr(self, p, obj[i]) for i, p in enumerate(self.props)]


class BirtCycleSite:
    def __init__(self, obj):
        self.props = 'index site_ref district road_number location_a location_b directions easting northing road_type authority sensor_id'
        if isinstance(obj, api.CycleSite):
            self._adapt(obj)
        else:
            [setattr(self, p, obj[i]) for i, p in enumerate(self.props.split(sep=' '))]

    def _adapt(self, obj):
        print('adapting')
        [setattr(self, p, None) for i, p in enumerate(self.props.split(sep=' '))]
        self.site_ref = obj.name
        self.location_a = obj.desc


class UpdateLogger:
    def __init__(self):
        self.errors = []
        self.old_data = []
        self.inserted = []
        self.nonexist = []

    def log_success(self, site):
        self.inserted.append(site.site_ref)

    def log_error(self, site):
        self.errors.append(site.site_ref)

    def log_old(self, site):
        self.old_data.append(site.site_ref)

    def log_nonexist(self, site):
        self.nonexist.append(site.site_ref)

    def total(self):
        # Return sum of length of each counter (sum of all API operations attempted)
        return sum(
            [len(value) for attr, value in self.__dict__.items()]
            )
            
    def summary(self):
        print("API Payload: {} sites".format(self.total()))
        print("Successfully added: {} sites".format(len(self.inserted)))
        print("No data for {} sites".format(len(self.errors)))
        print("Older data for {} sites".format(len(self.old_data)))
        print("{} sites from API missing in DB".format(len(self.nonexist)))


class CycleDb:
    def __init__(self, host=dbauth['host'], port=5432):
        self.conn = psycopg2.connect(database="birt_bcc", user=dbauth['usr'], password=dbauth['pw'], host=host, port=port)
        self.pg = self.conn.cursor()
        
    def get_sites(self):
        self.pg.execute("""SELECT * FROM cycle_counters;""")
        colnames = [desc[0] for desc in self.pg.description]
        payload = self.pg.fetchall()
        return [BirtCycleSite(site) for site in payload]

    def get_data(self, site_ref, start, end, direction='both'):
        pass

    def get_latest_samples(self):
        # Gets latest datapoint for each cycling site and returns a stack of BirtCycleSamples
        q = "SELECT sensor_id, max(update_time) FROM cycle_data GROUP BY sensor_id ORDER BY sensor_id;" 
        self.pg.execute(q)
        return {sensor_id : update_time for sensor_id, update_time in self.pg.fetchall()}
         
    def insert_day(self, site, data):
        query = """INSERT INTO cycle_data (sensor_id, flow, direction, missing, update_time) 
            VALUES ( %s, %s, %s, %s, %s )
            """
        params = []

        # Create a row for every hour bin, for every channel in sensor data
        if not data.error:
            for channel in data.flow:
                for hour, value in channel.data.items():
 
                    params.append([
                        site.sensor_id,
                        value,
                        channel.direction,
                        False,
                        datetime(channel.date.year, channel.date.month, channel.date.day, hour, 0, 0)
                    ])

        # use a batch statement to insert into DB
        execute_batch(self.pg, query, params)
        

def mainloop():
    # Time the process
    t = datetime.now()

    with open('config.json',"r") as j:
        auth=json.load(j)

    # Initialise cycling API object and DB object
    cycle_api = api.CycleApi(auth)
    db = CycleDb()
    logger = UpdateLogger()

    # Get the cycling sites and latest samples for each site in DB
    db_sites = db.get_sites()
    latest_samples = db.get_latest_samples()

    # For each site from cycling API
    for cycle_site in cycle_api.sites:
        # Find if site already exists in db
        db_site = [dbsite for dbsite in db_sites if cycle_site.site_ref == dbsite.site_ref]
        
        if db_site:
            # Get last day's data
            db_site = db_site[-1]
            data = cycle_api.get_data(cycle_site, datetime.today() - timedelta(days=1), datetime.today())
            # data = cycle_api.get_data(cycle_site, datetime(2018,1,1,0,0,0), datetime(2018,1,2,0,0,0))

            # Check if data contains errors
            if data.error:
                logger.log_error(cycle_site)
                continue

            # Verify data isn't older than samples in DB
            else:
                # Check if data from API is newer than latest update_time for each sensor (for each channel)
                if all([channel.date > latest_samples[db_site.sensor_id].date() for channel in data.flow]):
                    # Insert into DB
                    db.insert_day(db_site, data)
                    logger.log_success(cycle_site)
                else:
                    logger.log_old(cycle_site)
        else:
            # This cycling site doesn't exist in our database - handle this if necessary
            logger.log_nonexist(cycle_site)


    logger.summary()
    # db.conn.commit()
    # db.conn.close()
    
    print('Elapsed:', datetime.now()-t)

if __name__ == "__main__":
    mainloop()
    