import json
import datetime

from cycleapi import CycleApi, CycleData

TOKEN_URL="https://Vdanet-beta.ca-traffic.com/Token"
LIST_URL="https://vdanet-beta.ca-traffic.com/api/SiteList/VDAPro-TransportForWestMids/All/NULL/1970-1-1/1970-1-1"
COUNTER_URL="https://vdanet-beta.ca-traffic.com/api/FlowData/VDAPro-TransportForWestMids/"
HEADER={'content-type':'application/x-www-form-urlencoded'}

with open('config.json',"r") as j:
	auth=json.load(j)

c = CycleApi(auth)




sites_and_data = []

for site in c.sites:
    result = c.get_data(site, datetime.datetime(2018,1,1,0,0,0), datetime.datetime(2018,1,2,0,0,0))
    sites_and_data.append(result)
