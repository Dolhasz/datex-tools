#!/usr/bin/env python

#
# Generated Tue Feb 12 13:49:52 2019 by generateDS.py version 2.30.13.
# Python 3.5.4 |Anaconda, Inc.| (default, Nov  8 2017, 14:34:30) [MSC v.1900 64 bit (AMD64)]
#
# Command line options:
#   ('-o', 'DATEX.py')
#   ('-s', 'DATEX_STUBS.py')
#
# Command line arguments:
#   .\DATEXIISchema_2_2_0.xsd
#
# Command line:
#   .\generateDS.py -o "DATEX.py" -s "DATEX_STUBS.py" .\DATEXIISchema_2_2_0.xsd
#
# Current working directory (os.getcwd()):
#   generateds
#

import os
import sys
from lxml import etree as etree_

import ??? as supermod

def parsexml_(infile, parser=None, **kwargs):
    if parser is None:
        # Use the lxml ElementTree compatible parser so that, e.g.,
        #   we ignore comments.
        parser = etree_.ETCompatXMLParser()
    doc = etree_.parse(os.path.join(infile), parser=parser, **kwargs)
    return doc

#
# Globals
#

ExternalEncoding = ''

#
# Data representation classes
#


class _ExtensionTypeSub(supermod._ExtensionType):
    def __init__(self, anytypeobjs_=None, **kwargs_):
        super(_ExtensionTypeSub, self).__init__(anytypeobjs_,  **kwargs_)
supermod._ExtensionType.subclass = _ExtensionTypeSub
# end class _ExtensionTypeSub


class _IntermediatePointOnLinearElementSub(supermod._IntermediatePointOnLinearElement):
    def __init__(self, index=None, referent=None, **kwargs_):
        super(_IntermediatePointOnLinearElementSub, self).__init__(index, referent,  **kwargs_)
supermod._IntermediatePointOnLinearElement.subclass = _IntermediatePointOnLinearElementSub
# end class _IntermediatePointOnLinearElementSub


class _LocationContainedInItinerarySub(supermod._LocationContainedInItinerary):
    def __init__(self, index=None, location=None, **kwargs_):
        super(_LocationContainedInItinerarySub, self).__init__(index, location,  **kwargs_)
supermod._LocationContainedInItinerary.subclass = _LocationContainedInItinerarySub
# end class _LocationContainedInItinerarySub


class _MeasurementSiteRecordIndexMeasurementSpecificCharacteristicsSub(supermod._MeasurementSiteRecordIndexMeasurementSpecificCharacteristics):
    def __init__(self, index=None, measurementSpecificCharacteristics=None, **kwargs_):
        super(_MeasurementSiteRecordIndexMeasurementSpecificCharacteristicsSub, self).__init__(index, measurementSpecificCharacteristics,  **kwargs_)
supermod._MeasurementSiteRecordIndexMeasurementSpecificCharacteristics.subclass = _MeasurementSiteRecordIndexMeasurementSpecificCharacteristicsSub
# end class _MeasurementSiteRecordIndexMeasurementSpecificCharacteristicsSub


class _PredefinedItineraryIndexPredefinedLocationSub(supermod._PredefinedItineraryIndexPredefinedLocation):
    def __init__(self, index=None, predefinedLocation=None, **kwargs_):
        super(_PredefinedItineraryIndexPredefinedLocationSub, self).__init__(index, predefinedLocation,  **kwargs_)
supermod._PredefinedItineraryIndexPredefinedLocation.subclass = _PredefinedItineraryIndexPredefinedLocationSub
# end class _PredefinedItineraryIndexPredefinedLocationSub


class _SiteMeasurementsIndexMeasuredValueSub(supermod._SiteMeasurementsIndexMeasuredValue):
    def __init__(self, index=None, measuredValue=None, **kwargs_):
        super(_SiteMeasurementsIndexMeasuredValueSub, self).__init__(index, measuredValue,  **kwargs_)
supermod._SiteMeasurementsIndexMeasuredValue.subclass = _SiteMeasurementsIndexMeasuredValueSub
# end class _SiteMeasurementsIndexMeasuredValueSub


class _TextPageSub(supermod._TextPage):
    def __init__(self, pageNumber=None, vmsText=None, **kwargs_):
        super(_TextPageSub, self).__init__(pageNumber, vmsText,  **kwargs_)
supermod._TextPage.subclass = _TextPageSub
# end class _TextPageSub


class _VmsDynamicCharacteristicsPictogramDisplayAreaIndexVmsPictogramDisplayCharacteristicsSub(supermod._VmsDynamicCharacteristicsPictogramDisplayAreaIndexVmsPictogramDisplayCharacteristics):
    def __init__(self, pictogramDisplayAreaIndex=None, vmsPictogramDisplayCharacteristics=None, **kwargs_):
        super(_VmsDynamicCharacteristicsPictogramDisplayAreaIndexVmsPictogramDisplayCharacteristicsSub, self).__init__(pictogramDisplayAreaIndex, vmsPictogramDisplayCharacteristics,  **kwargs_)
supermod._VmsDynamicCharacteristicsPictogramDisplayAreaIndexVmsPictogramDisplayCharacteristics.subclass = _VmsDynamicCharacteristicsPictogramDisplayAreaIndexVmsPictogramDisplayCharacteristicsSub
# end class _VmsDynamicCharacteristicsPictogramDisplayAreaIndexVmsPictogramDisplayCharacteristicsSub


class _VmsMessageIndexVmsMessageSub(supermod._VmsMessageIndexVmsMessage):
    def __init__(self, messageIndex=None, vmsMessage=None, **kwargs_):
        super(_VmsMessageIndexVmsMessageSub, self).__init__(messageIndex, vmsMessage,  **kwargs_)
supermod._VmsMessageIndexVmsMessage.subclass = _VmsMessageIndexVmsMessageSub
# end class _VmsMessageIndexVmsMessageSub


class _VmsMessagePictogramDisplayAreaIndexVmsPictogramDisplayAreaSub(supermod._VmsMessagePictogramDisplayAreaIndexVmsPictogramDisplayArea):
    def __init__(self, pictogramDisplayAreaIndex=None, vmsPictogramDisplayArea=None, **kwargs_):
        super(_VmsMessagePictogramDisplayAreaIndexVmsPictogramDisplayAreaSub, self).__init__(pictogramDisplayAreaIndex, vmsPictogramDisplayArea,  **kwargs_)
supermod._VmsMessagePictogramDisplayAreaIndexVmsPictogramDisplayArea.subclass = _VmsMessagePictogramDisplayAreaIndexVmsPictogramDisplayAreaSub
# end class _VmsMessagePictogramDisplayAreaIndexVmsPictogramDisplayAreaSub


class _VmsPictogramDisplayAreaIndexPictogramDisplayAreaSettingsSub(supermod._VmsPictogramDisplayAreaIndexPictogramDisplayAreaSettings):
    def __init__(self, pictogramDisplayAreaIndex=None, pictogramDisplayAreaSettings=None, **kwargs_):
        super(_VmsPictogramDisplayAreaIndexPictogramDisplayAreaSettingsSub, self).__init__(pictogramDisplayAreaIndex, pictogramDisplayAreaSettings,  **kwargs_)
supermod._VmsPictogramDisplayAreaIndexPictogramDisplayAreaSettings.subclass = _VmsPictogramDisplayAreaIndexPictogramDisplayAreaSettingsSub
# end class _VmsPictogramDisplayAreaIndexPictogramDisplayAreaSettingsSub


class _VmsPictogramDisplayAreaPictogramSequencingIndexVmsPictogramSub(supermod._VmsPictogramDisplayAreaPictogramSequencingIndexVmsPictogram):
    def __init__(self, pictogramSequencingIndex=None, vmsPictogram=None, **kwargs_):
        super(_VmsPictogramDisplayAreaPictogramSequencingIndexVmsPictogramSub, self).__init__(pictogramSequencingIndex, vmsPictogram,  **kwargs_)
supermod._VmsPictogramDisplayAreaPictogramSequencingIndexVmsPictogram.subclass = _VmsPictogramDisplayAreaPictogramSequencingIndexVmsPictogramSub
# end class _VmsPictogramDisplayAreaPictogramSequencingIndexVmsPictogramSub


class _VmsRecordPictogramDisplayAreaIndexVmsPictogramDisplayCharacteristicsSub(supermod._VmsRecordPictogramDisplayAreaIndexVmsPictogramDisplayCharacteristics):
    def __init__(self, pictogramDisplayAreaIndex=None, vmsPictogramDisplayCharacteristics=None, **kwargs_):
        super(_VmsRecordPictogramDisplayAreaIndexVmsPictogramDisplayCharacteristicsSub, self).__init__(pictogramDisplayAreaIndex, vmsPictogramDisplayCharacteristics,  **kwargs_)
supermod._VmsRecordPictogramDisplayAreaIndexVmsPictogramDisplayCharacteristics.subclass = _VmsRecordPictogramDisplayAreaIndexVmsPictogramDisplayCharacteristicsSub
# end class _VmsRecordPictogramDisplayAreaIndexVmsPictogramDisplayCharacteristicsSub


class _VmsTextLineIndexVmsTextLineSub(supermod._VmsTextLineIndexVmsTextLine):
    def __init__(self, lineIndex=None, vmsTextLine=None, **kwargs_):
        super(_VmsTextLineIndexVmsTextLineSub, self).__init__(lineIndex, vmsTextLine,  **kwargs_)
supermod._VmsTextLineIndexVmsTextLine.subclass = _VmsTextLineIndexVmsTextLineSub
# end class _VmsTextLineIndexVmsTextLineSub


class _VmsUnitRecordVmsIndexVmsRecordSub(supermod._VmsUnitRecordVmsIndexVmsRecord):
    def __init__(self, vmsIndex=None, vmsRecord=None, **kwargs_):
        super(_VmsUnitRecordVmsIndexVmsRecordSub, self).__init__(vmsIndex, vmsRecord,  **kwargs_)
supermod._VmsUnitRecordVmsIndexVmsRecord.subclass = _VmsUnitRecordVmsIndexVmsRecordSub
# end class _VmsUnitRecordVmsIndexVmsRecordSub


class _VmsUnitVmsIndexVmsSub(supermod._VmsUnitVmsIndexVms):
    def __init__(self, vmsIndex=None, vms=None, **kwargs_):
        super(_VmsUnitVmsIndexVmsSub, self).__init__(vmsIndex, vms,  **kwargs_)
supermod._VmsUnitVmsIndexVms.subclass = _VmsUnitVmsIndexVmsSub
# end class _VmsUnitVmsIndexVmsSub


class AffectedCarriagewayAndLanesSub(supermod.AffectedCarriagewayAndLanes):
    def __init__(self, carriageway=None, lane=None, footpath=None, lengthAffected=None, affectedCarriagewayAndLanesExtension=None, **kwargs_):
        super(AffectedCarriagewayAndLanesSub, self).__init__(carriageway, lane, footpath, lengthAffected, affectedCarriagewayAndLanesExtension,  **kwargs_)
supermod.AffectedCarriagewayAndLanes.subclass = AffectedCarriagewayAndLanesSub
# end class AffectedCarriagewayAndLanesSub


class AlertCAreaSub(supermod.AlertCArea):
    def __init__(self, alertCLocationCountryCode=None, alertCLocationTableNumber=None, alertCLocationTableVersion=None, areaLocation=None, alertCAreaExtension=None, **kwargs_):
        super(AlertCAreaSub, self).__init__(alertCLocationCountryCode, alertCLocationTableNumber, alertCLocationTableVersion, areaLocation, alertCAreaExtension,  **kwargs_)
supermod.AlertCArea.subclass = AlertCAreaSub
# end class AlertCAreaSub


class AlertCDirectionSub(supermod.AlertCDirection):
    def __init__(self, alertCDirectionCoded=None, alertCDirectionNamed=None, alertCDirectionSense=None, alertCDirectionExtension=None, **kwargs_):
        super(AlertCDirectionSub, self).__init__(alertCDirectionCoded, alertCDirectionNamed, alertCDirectionSense, alertCDirectionExtension,  **kwargs_)
supermod.AlertCDirection.subclass = AlertCDirectionSub
# end class AlertCDirectionSub


class AlertCLinearSub(supermod.AlertCLinear):
    def __init__(self, alertCLocationCountryCode=None, alertCLocationTableNumber=None, alertCLocationTableVersion=None, alertCLinearExtension=None, extensiontype_=None, **kwargs_):
        super(AlertCLinearSub, self).__init__(alertCLocationCountryCode, alertCLocationTableNumber, alertCLocationTableVersion, alertCLinearExtension, extensiontype_,  **kwargs_)
supermod.AlertCLinear.subclass = AlertCLinearSub
# end class AlertCLinearSub


class AlertCLinearByCodeSub(supermod.AlertCLinearByCode):
    def __init__(self, alertCLocationCountryCode=None, alertCLocationTableNumber=None, alertCLocationTableVersion=None, alertCLinearExtension=None, alertCDirection=None, locationCodeForLinearLocation=None, alertCLinearByCodeExtension=None, **kwargs_):
        super(AlertCLinearByCodeSub, self).__init__(alertCLocationCountryCode, alertCLocationTableNumber, alertCLocationTableVersion, alertCLinearExtension, alertCDirection, locationCodeForLinearLocation, alertCLinearByCodeExtension,  **kwargs_)
supermod.AlertCLinearByCode.subclass = AlertCLinearByCodeSub
# end class AlertCLinearByCodeSub


class AlertCLocationSub(supermod.AlertCLocation):
    def __init__(self, alertCLocationName=None, specificLocation=None, alertCLocationExtension=None, **kwargs_):
        super(AlertCLocationSub, self).__init__(alertCLocationName, specificLocation, alertCLocationExtension,  **kwargs_)
supermod.AlertCLocation.subclass = AlertCLocationSub
# end class AlertCLocationSub


class AlertCMethod2LinearSub(supermod.AlertCMethod2Linear):
    def __init__(self, alertCLocationCountryCode=None, alertCLocationTableNumber=None, alertCLocationTableVersion=None, alertCLinearExtension=None, alertCDirection=None, alertCMethod2PrimaryPointLocation=None, alertCMethod2SecondaryPointLocation=None, alertCMethod2LinearExtension=None, **kwargs_):
        super(AlertCMethod2LinearSub, self).__init__(alertCLocationCountryCode, alertCLocationTableNumber, alertCLocationTableVersion, alertCLinearExtension, alertCDirection, alertCMethod2PrimaryPointLocation, alertCMethod2SecondaryPointLocation, alertCMethod2LinearExtension,  **kwargs_)
supermod.AlertCMethod2Linear.subclass = AlertCMethod2LinearSub
# end class AlertCMethod2LinearSub


class AlertCMethod2PrimaryPointLocationSub(supermod.AlertCMethod2PrimaryPointLocation):
    def __init__(self, alertCLocation=None, alertCMethod2PrimaryPointLocationExtension=None, **kwargs_):
        super(AlertCMethod2PrimaryPointLocationSub, self).__init__(alertCLocation, alertCMethod2PrimaryPointLocationExtension,  **kwargs_)
supermod.AlertCMethod2PrimaryPointLocation.subclass = AlertCMethod2PrimaryPointLocationSub
# end class AlertCMethod2PrimaryPointLocationSub


class AlertCMethod2SecondaryPointLocationSub(supermod.AlertCMethod2SecondaryPointLocation):
    def __init__(self, alertCLocation=None, alertCMethod2SecondaryPointLocationExtension=None, **kwargs_):
        super(AlertCMethod2SecondaryPointLocationSub, self).__init__(alertCLocation, alertCMethod2SecondaryPointLocationExtension,  **kwargs_)
supermod.AlertCMethod2SecondaryPointLocation.subclass = AlertCMethod2SecondaryPointLocationSub
# end class AlertCMethod2SecondaryPointLocationSub


class AlertCMethod4LinearSub(supermod.AlertCMethod4Linear):
    def __init__(self, alertCLocationCountryCode=None, alertCLocationTableNumber=None, alertCLocationTableVersion=None, alertCLinearExtension=None, alertCDirection=None, alertCMethod4PrimaryPointLocation=None, alertCMethod4SecondaryPointLocation=None, alertCMethod4LinearExtension=None, **kwargs_):
        super(AlertCMethod4LinearSub, self).__init__(alertCLocationCountryCode, alertCLocationTableNumber, alertCLocationTableVersion, alertCLinearExtension, alertCDirection, alertCMethod4PrimaryPointLocation, alertCMethod4SecondaryPointLocation, alertCMethod4LinearExtension,  **kwargs_)
supermod.AlertCMethod4Linear.subclass = AlertCMethod4LinearSub
# end class AlertCMethod4LinearSub


class AlertCMethod4PrimaryPointLocationSub(supermod.AlertCMethod4PrimaryPointLocation):
    def __init__(self, alertCLocation=None, offsetDistance=None, alertCMethod4PrimaryPointLocationExtension=None, **kwargs_):
        super(AlertCMethod4PrimaryPointLocationSub, self).__init__(alertCLocation, offsetDistance, alertCMethod4PrimaryPointLocationExtension,  **kwargs_)
supermod.AlertCMethod4PrimaryPointLocation.subclass = AlertCMethod4PrimaryPointLocationSub
# end class AlertCMethod4PrimaryPointLocationSub


class AlertCMethod4SecondaryPointLocationSub(supermod.AlertCMethod4SecondaryPointLocation):
    def __init__(self, alertCLocation=None, offsetDistance=None, alertCMethod4SecondaryPointLocationExtension=None, **kwargs_):
        super(AlertCMethod4SecondaryPointLocationSub, self).__init__(alertCLocation, offsetDistance, alertCMethod4SecondaryPointLocationExtension,  **kwargs_)
supermod.AlertCMethod4SecondaryPointLocation.subclass = AlertCMethod4SecondaryPointLocationSub
# end class AlertCMethod4SecondaryPointLocationSub


class AlertCPointSub(supermod.AlertCPoint):
    def __init__(self, alertCLocationCountryCode=None, alertCLocationTableNumber=None, alertCLocationTableVersion=None, alertCPointExtension=None, extensiontype_=None, **kwargs_):
        super(AlertCPointSub, self).__init__(alertCLocationCountryCode, alertCLocationTableNumber, alertCLocationTableVersion, alertCPointExtension, extensiontype_,  **kwargs_)
supermod.AlertCPoint.subclass = AlertCPointSub
# end class AlertCPointSub


class AxleSpacingSub(supermod.AxleSpacing):
    def __init__(self, axleSpacing=None, axleSpacingSequenceIdentifier=None, axleSpacingExtension=None, **kwargs_):
        super(AxleSpacingSub, self).__init__(axleSpacing, axleSpacingSequenceIdentifier, axleSpacingExtension,  **kwargs_)
supermod.AxleSpacing.subclass = AxleSpacingSub
# end class AxleSpacingSub


class AxleWeightSub(supermod.AxleWeight):
    def __init__(self, axlePositionIdentifier=None, axleWeight=None, maximumPermittedAxleWeight=None, axleWeightExtension=None, **kwargs_):
        super(AxleWeightSub, self).__init__(axlePositionIdentifier, axleWeight, maximumPermittedAxleWeight, axleWeightExtension,  **kwargs_)
supermod.AxleWeight.subclass = AxleWeightSub
# end class AxleWeightSub


class BasicDataSub(supermod.BasicData):
    def __init__(self, measurementOrCalculatedTimePrecision=None, measurementOrCalculationPeriod=None, measurementOrCalculationTime=None, pertinentLocation=None, basicDataExtension=None, extensiontype_=None, **kwargs_):
        super(BasicDataSub, self).__init__(measurementOrCalculatedTimePrecision, measurementOrCalculationPeriod, measurementOrCalculationTime, pertinentLocation, basicDataExtension, extensiontype_,  **kwargs_)
supermod.BasicData.subclass = BasicDataSub
# end class BasicDataSub


class CatalogueReferenceSub(supermod.CatalogueReference):
    def __init__(self, keyCatalogueReference=None, catalogueReferenceExtension=None, **kwargs_):
        super(CatalogueReferenceSub, self).__init__(keyCatalogueReference, catalogueReferenceExtension,  **kwargs_)
supermod.CatalogueReference.subclass = CatalogueReferenceSub
# end class CatalogueReferenceSub


class CauseSub(supermod.Cause):
    def __init__(self, causeExtension=None, extensiontype_=None, **kwargs_):
        super(CauseSub, self).__init__(causeExtension, extensiontype_,  **kwargs_)
supermod.Cause.subclass = CauseSub
# end class CauseSub


class CommentSub(supermod.Comment):
    def __init__(self, comment=None, commentDateTime=None, commentType=None, commentExtension=None, **kwargs_):
        super(CommentSub, self).__init__(comment, commentDateTime, commentType, commentExtension,  **kwargs_)
supermod.Comment.subclass = CommentSub
# end class CommentSub


class D2LogicalModelSub(supermod.D2LogicalModel):
    def __init__(self, modelBaseVersion='2', exchange=None, payloadPublication=None, d2LogicalModelExtension=None, **kwargs_):
        super(D2LogicalModelSub, self).__init__(modelBaseVersion, exchange, payloadPublication, d2LogicalModelExtension,  **kwargs_)
supermod.D2LogicalModel.subclass = D2LogicalModelSub
# end class D2LogicalModelSub


class DataValueSub(supermod.DataValue):
    def __init__(self, accuracy=None, computationalMethod=None, numberOfIncompleteInputs=None, numberOfInputValuesUsed=None, smoothingFactor=None, standardDeviation=None, supplierCalculatedDataQuality=None, dataError=None, reasonForDataError=None, dataValueExtension=None, extensiontype_=None, **kwargs_):
        super(DataValueSub, self).__init__(accuracy, computationalMethod, numberOfIncompleteInputs, numberOfInputValuesUsed, smoothingFactor, standardDeviation, supplierCalculatedDataQuality, dataError, reasonForDataError, dataValueExtension, extensiontype_,  **kwargs_)
supermod.DataValue.subclass = DataValueSub
# end class DataValueSub


class DateTimeValueSub(supermod.DateTimeValue):
    def __init__(self, accuracy=None, computationalMethod=None, numberOfIncompleteInputs=None, numberOfInputValuesUsed=None, smoothingFactor=None, standardDeviation=None, supplierCalculatedDataQuality=None, dataError=None, reasonForDataError=None, dataValueExtension=None, dateTime=None, dateTimeValueExtension=None, **kwargs_):
        super(DateTimeValueSub, self).__init__(accuracy, computationalMethod, numberOfIncompleteInputs, numberOfInputValuesUsed, smoothingFactor, standardDeviation, supplierCalculatedDataQuality, dataError, reasonForDataError, dataValueExtension, dateTime, dateTimeValueExtension,  **kwargs_)
supermod.DateTimeValue.subclass = DateTimeValueSub
# end class DateTimeValueSub


class DayWeekMonthSub(supermod.DayWeekMonth):
    def __init__(self, applicableDay=None, applicableWeek=None, applicableMonth=None, dayWeekMonthExtension=None, **kwargs_):
        super(DayWeekMonthSub, self).__init__(applicableDay, applicableWeek, applicableMonth, dayWeekMonthExtension,  **kwargs_)
supermod.DayWeekMonth.subclass = DayWeekMonthSub
# end class DayWeekMonthSub


class DelaysSub(supermod.Delays):
    def __init__(self, delayBand=None, delaysType=None, delayTimeValue=None, delaysExtension=None, **kwargs_):
        super(DelaysSub, self).__init__(delayBand, delaysType, delayTimeValue, delaysExtension,  **kwargs_)
supermod.Delays.subclass = DelaysSub
# end class DelaysSub


class DestinationSub(supermod.Destination):
    def __init__(self, destinationExtension=None, extensiontype_=None, **kwargs_):
        super(DestinationSub, self).__init__(destinationExtension, extensiontype_,  **kwargs_)
supermod.Destination.subclass = DestinationSub
# end class DestinationSub


class DirectionBearingValueSub(supermod.DirectionBearingValue):
    def __init__(self, accuracy=None, computationalMethod=None, numberOfIncompleteInputs=None, numberOfInputValuesUsed=None, smoothingFactor=None, standardDeviation=None, supplierCalculatedDataQuality=None, dataError=None, reasonForDataError=None, dataValueExtension=None, directionBearing=None, directionBearingValueExtension=None, **kwargs_):
        super(DirectionBearingValueSub, self).__init__(accuracy, computationalMethod, numberOfIncompleteInputs, numberOfInputValuesUsed, smoothingFactor, standardDeviation, supplierCalculatedDataQuality, dataError, reasonForDataError, dataValueExtension, directionBearing, directionBearingValueExtension,  **kwargs_)
supermod.DirectionBearingValue.subclass = DirectionBearingValueSub
# end class DirectionBearingValueSub


class DirectionCompassValueSub(supermod.DirectionCompassValue):
    def __init__(self, accuracy=None, computationalMethod=None, numberOfIncompleteInputs=None, numberOfInputValuesUsed=None, smoothingFactor=None, standardDeviation=None, supplierCalculatedDataQuality=None, dataError=None, reasonForDataError=None, dataValueExtension=None, directionCompass=None, directionCompassValueExtension=None, **kwargs_):
        super(DirectionCompassValueSub, self).__init__(accuracy, computationalMethod, numberOfIncompleteInputs, numberOfInputValuesUsed, smoothingFactor, standardDeviation, supplierCalculatedDataQuality, dataError, reasonForDataError, dataValueExtension, directionCompass, directionCompassValueExtension,  **kwargs_)
supermod.DirectionCompassValue.subclass = DirectionCompassValueSub
# end class DirectionCompassValueSub


class DistanceAlongLinearElementSub(supermod.DistanceAlongLinearElement):
    def __init__(self, distanceAlongLinearElementExtension=None, extensiontype_=None, **kwargs_):
        super(DistanceAlongLinearElementSub, self).__init__(distanceAlongLinearElementExtension, extensiontype_,  **kwargs_)
supermod.DistanceAlongLinearElement.subclass = DistanceAlongLinearElementSub
# end class DistanceAlongLinearElementSub


class DistanceFromLinearElementReferentSub(supermod.DistanceFromLinearElementReferent):
    def __init__(self, distanceAlongLinearElementExtension=None, distanceAlong=None, fromReferent=None, towardsReferent=None, distanceFromLinearElementReferentExtension=None, **kwargs_):
        super(DistanceFromLinearElementReferentSub, self).__init__(distanceAlongLinearElementExtension, distanceAlong, fromReferent, towardsReferent, distanceFromLinearElementReferentExtension,  **kwargs_)
supermod.DistanceFromLinearElementReferent.subclass = DistanceFromLinearElementReferentSub
# end class DistanceFromLinearElementReferentSub


class DistanceFromLinearElementStartSub(supermod.DistanceFromLinearElementStart):
    def __init__(self, distanceAlongLinearElementExtension=None, distanceAlong=None, distanceFromLinearElementStartExtension=None, **kwargs_):
        super(DistanceFromLinearElementStartSub, self).__init__(distanceAlongLinearElementExtension, distanceAlong, distanceFromLinearElementStartExtension,  **kwargs_)
supermod.DistanceFromLinearElementStart.subclass = DistanceFromLinearElementStartSub
# end class DistanceFromLinearElementStartSub


class DurationValueSub(supermod.DurationValue):
    def __init__(self, accuracy=None, computationalMethod=None, numberOfIncompleteInputs=None, numberOfInputValuesUsed=None, smoothingFactor=None, standardDeviation=None, supplierCalculatedDataQuality=None, dataError=None, reasonForDataError=None, dataValueExtension=None, duration=None, durationValueExtension=None, **kwargs_):
        super(DurationValueSub, self).__init__(accuracy, computationalMethod, numberOfIncompleteInputs, numberOfInputValuesUsed, smoothingFactor, standardDeviation, supplierCalculatedDataQuality, dataError, reasonForDataError, dataValueExtension, duration, durationValueExtension,  **kwargs_)
supermod.DurationValue.subclass = DurationValueSub
# end class DurationValueSub


class ElaboratedDataSub(supermod.ElaboratedData):
    def __init__(self, forecast=None, source=None, validity=None, elaboratedDataFault=None, basicData=None, elaboratedDataExtension=None, **kwargs_):
        super(ElaboratedDataSub, self).__init__(forecast, source, validity, elaboratedDataFault, basicData, elaboratedDataExtension,  **kwargs_)
supermod.ElaboratedData.subclass = ElaboratedDataSub
# end class ElaboratedDataSub


class ExchangeSub(supermod.Exchange):
    def __init__(self, changedFlag=None, clientIdentification=None, deliveryBreak=None, denyReason=None, historicalStartDate=None, historicalStopDate=None, keepAlive=None, requestType=None, response=None, subscriptionReference=None, supplierIdentification=None, target=None, subscription=None, filterReference=None, catalogueReference=None, exchangeExtension=None, **kwargs_):
        super(ExchangeSub, self).__init__(changedFlag, clientIdentification, deliveryBreak, denyReason, historicalStartDate, historicalStopDate, keepAlive, requestType, response, subscriptionReference, supplierIdentification, target, subscription, filterReference, catalogueReference, exchangeExtension,  **kwargs_)
supermod.Exchange.subclass = ExchangeSub
# end class ExchangeSub


class ExternalReferencingSub(supermod.ExternalReferencing):
    def __init__(self, externalLocationCode=None, externalReferencingSystem=None, externalReferencingExtension=None, **kwargs_):
        super(ExternalReferencingSub, self).__init__(externalLocationCode, externalReferencingSystem, externalReferencingExtension,  **kwargs_)
supermod.ExternalReferencing.subclass = ExternalReferencingSub
# end class ExternalReferencingSub


class FaultSub(supermod.Fault):
    def __init__(self, faultIdentifier=None, faultDescription=None, faultCreationTime=None, faultLastUpdateTime=None, faultSeverity=None, faultExtension=None, extensiontype_=None, **kwargs_):
        super(FaultSub, self).__init__(faultIdentifier, faultDescription, faultCreationTime, faultLastUpdateTime, faultSeverity, faultExtension, extensiontype_,  **kwargs_)
supermod.Fault.subclass = FaultSub
# end class FaultSub


class FilterExitManagementSub(supermod.FilterExitManagement):
    def __init__(self, filterEnd=None, filterOutOfRange=None, filterExitManagementExtension=None, **kwargs_):
        super(FilterExitManagementSub, self).__init__(filterEnd, filterOutOfRange, filterExitManagementExtension,  **kwargs_)
supermod.FilterExitManagement.subclass = FilterExitManagementSub
# end class FilterExitManagementSub


class FilterReferenceSub(supermod.FilterReference):
    def __init__(self, deleteFilter=None, filterOperationApproved=None, keyFilterReference=None, filterReferenceExtension=None, **kwargs_):
        super(FilterReferenceSub, self).__init__(deleteFilter, filterOperationApproved, keyFilterReference, filterReferenceExtension,  **kwargs_)
supermod.FilterReference.subclass = FilterReferenceSub
# end class FilterReferenceSub


class FloatingPointMetreDistanceValueSub(supermod.FloatingPointMetreDistanceValue):
    def __init__(self, accuracy=None, computationalMethod=None, numberOfIncompleteInputs=None, numberOfInputValuesUsed=None, smoothingFactor=None, standardDeviation=None, supplierCalculatedDataQuality=None, dataError=None, reasonForDataError=None, dataValueExtension=None, floatingPointMetreDistance=None, floatingPointMetreDistanceValueExtension=None, **kwargs_):
        super(FloatingPointMetreDistanceValueSub, self).__init__(accuracy, computationalMethod, numberOfIncompleteInputs, numberOfInputValuesUsed, smoothingFactor, standardDeviation, supplierCalculatedDataQuality, dataError, reasonForDataError, dataValueExtension, floatingPointMetreDistance, floatingPointMetreDistanceValueExtension,  **kwargs_)
supermod.FloatingPointMetreDistanceValue.subclass = FloatingPointMetreDistanceValueSub
# end class FloatingPointMetreDistanceValueSub


class GrossWeightCharacteristicSub(supermod.GrossWeightCharacteristic):
    def __init__(self, comparisonOperator=None, grossVehicleWeight=None, grossWeightCharacteristicExtension=None, **kwargs_):
        super(GrossWeightCharacteristicSub, self).__init__(comparisonOperator, grossVehicleWeight, grossWeightCharacteristicExtension,  **kwargs_)
supermod.GrossWeightCharacteristic.subclass = GrossWeightCharacteristicSub
# end class GrossWeightCharacteristicSub


class GroupOfLocationsSub(supermod.GroupOfLocations):
    def __init__(self, groupOfLocationsExtension=None, extensiontype_=None, **kwargs_):
        super(GroupOfLocationsSub, self).__init__(groupOfLocationsExtension, extensiontype_,  **kwargs_)
supermod.GroupOfLocations.subclass = GroupOfLocationsSub
# end class GroupOfLocationsSub


class GroupOfPeopleInvolvedSub(supermod.GroupOfPeopleInvolved):
    def __init__(self, numberOfPeople=None, injuryStatus=None, involvementRole=None, categoryOfPeopleInvolved=None, groupOfPeopleInvolvedExtension=None, **kwargs_):
        super(GroupOfPeopleInvolvedSub, self).__init__(numberOfPeople, injuryStatus, involvementRole, categoryOfPeopleInvolved, groupOfPeopleInvolvedExtension,  **kwargs_)
supermod.GroupOfPeopleInvolved.subclass = GroupOfPeopleInvolvedSub
# end class GroupOfPeopleInvolvedSub


class GroupOfVehiclesInvolvedSub(supermod.GroupOfVehiclesInvolved):
    def __init__(self, numberOfVehicles=None, vehicleStatus=None, vehicleCharacteristics=None, groupOfVehiclesInvolvedExtension=None, **kwargs_):
        super(GroupOfVehiclesInvolvedSub, self).__init__(numberOfVehicles, vehicleStatus, vehicleCharacteristics, groupOfVehiclesInvolvedExtension,  **kwargs_)
supermod.GroupOfVehiclesInvolved.subclass = GroupOfVehiclesInvolvedSub
# end class GroupOfVehiclesInvolvedSub


class HazardousMaterialsSub(supermod.HazardousMaterials):
    def __init__(self, chemicalName=None, dangerousGoodsFlashPoint=None, dangerousGoodsRegulations=None, hazardCodeIdentification=None, hazardCodeVersionNumber=None, hazardSubstanceItemPageNumber=None, tremCardNumber=None, undgNumber=None, volumeOfDangerousGoods=None, weightOfDangerousGoods=None, hazardousMaterialsExtension=None, **kwargs_):
        super(HazardousMaterialsSub, self).__init__(chemicalName, dangerousGoodsFlashPoint, dangerousGoodsRegulations, hazardCodeIdentification, hazardCodeVersionNumber, hazardSubstanceItemPageNumber, tremCardNumber, undgNumber, volumeOfDangerousGoods, weightOfDangerousGoods, hazardousMaterialsExtension,  **kwargs_)
supermod.HazardousMaterials.subclass = HazardousMaterialsSub
# end class HazardousMaterialsSub


class HeaderInformationSub(supermod.HeaderInformation):
    def __init__(self, areaOfInterest=None, confidentiality=None, informationStatus=None, urgency=None, headerInformationExtension=None, **kwargs_):
        super(HeaderInformationSub, self).__init__(areaOfInterest, confidentiality, informationStatus, urgency, headerInformationExtension,  **kwargs_)
supermod.HeaderInformation.subclass = HeaderInformationSub
# end class HeaderInformationSub


class HeaviestAxleWeightCharacteristicSub(supermod.HeaviestAxleWeightCharacteristic):
    def __init__(self, comparisonOperator=None, heaviestAxleWeight=None, heaviestAxleWeightCharacteristicExtension=None, **kwargs_):
        super(HeaviestAxleWeightCharacteristicSub, self).__init__(comparisonOperator, heaviestAxleWeight, heaviestAxleWeightCharacteristicExtension,  **kwargs_)
supermod.HeaviestAxleWeightCharacteristic.subclass = HeaviestAxleWeightCharacteristicSub
# end class HeaviestAxleWeightCharacteristicSub


class HeightCharacteristicSub(supermod.HeightCharacteristic):
    def __init__(self, comparisonOperator=None, vehicleHeight=None, heightCharacteristicExtension=None, **kwargs_):
        super(HeightCharacteristicSub, self).__init__(comparisonOperator, vehicleHeight, heightCharacteristicExtension,  **kwargs_)
supermod.HeightCharacteristic.subclass = HeightCharacteristicSub
# end class HeightCharacteristicSub


class HumiditySub(supermod.Humidity):
    def __init__(self, relativeHumidity=None, humidityExtension=None, **kwargs_):
        super(HumiditySub, self).__init__(relativeHumidity, humidityExtension,  **kwargs_)
supermod.Humidity.subclass = HumiditySub
# end class HumiditySub


class ImpactSub(supermod.Impact):
    def __init__(self, capacityRemaining=None, numberOfLanesRestricted=None, numberOfOperationalLanes=None, originalNumberOfLanes=None, residualRoadWidth=None, trafficConstrictionType=None, delays=None, impactExtension=None, **kwargs_):
        super(ImpactSub, self).__init__(capacityRemaining, numberOfLanesRestricted, numberOfOperationalLanes, originalNumberOfLanes, residualRoadWidth, trafficConstrictionType, delays, impactExtension,  **kwargs_)
supermod.Impact.subclass = ImpactSub
# end class ImpactSub


class IntegerMetreDistanceValueSub(supermod.IntegerMetreDistanceValue):
    def __init__(self, accuracy=None, computationalMethod=None, numberOfIncompleteInputs=None, numberOfInputValuesUsed=None, smoothingFactor=None, standardDeviation=None, supplierCalculatedDataQuality=None, dataError=None, reasonForDataError=None, dataValueExtension=None, integerMetreDistance=None, integerMetreDistanceValueExtension=None, **kwargs_):
        super(IntegerMetreDistanceValueSub, self).__init__(accuracy, computationalMethod, numberOfIncompleteInputs, numberOfInputValuesUsed, smoothingFactor, standardDeviation, supplierCalculatedDataQuality, dataError, reasonForDataError, dataValueExtension, integerMetreDistance, integerMetreDistanceValueExtension,  **kwargs_)
supermod.IntegerMetreDistanceValue.subclass = IntegerMetreDistanceValueSub
# end class IntegerMetreDistanceValueSub


class InternationalIdentifierSub(supermod.InternationalIdentifier):
    def __init__(self, country=None, nationalIdentifier=None, internationalIdentifierExtension=None, **kwargs_):
        super(InternationalIdentifierSub, self).__init__(country, nationalIdentifier, internationalIdentifierExtension,  **kwargs_)
supermod.InternationalIdentifier.subclass = InternationalIdentifierSub
# end class InternationalIdentifierSub


class ItinerarySub(supermod.Itinerary):
    def __init__(self, groupOfLocationsExtension=None, routeDestination=None, itineraryExtension=None, extensiontype_=None, **kwargs_):
        super(ItinerarySub, self).__init__(groupOfLocationsExtension, routeDestination, itineraryExtension, extensiontype_,  **kwargs_)
supermod.Itinerary.subclass = ItinerarySub
# end class ItinerarySub


class ItineraryByIndexedLocationsSub(supermod.ItineraryByIndexedLocations):
    def __init__(self, groupOfLocationsExtension=None, routeDestination=None, itineraryExtension=None, locationContainedInItinerary=None, itineraryByIndexedLocationsExtension=None, **kwargs_):
        super(ItineraryByIndexedLocationsSub, self).__init__(groupOfLocationsExtension, routeDestination, itineraryExtension, locationContainedInItinerary, itineraryByIndexedLocationsExtension,  **kwargs_)
supermod.ItineraryByIndexedLocations.subclass = ItineraryByIndexedLocationsSub
# end class ItineraryByIndexedLocationsSub


class ItineraryByReferenceSub(supermod.ItineraryByReference):
    def __init__(self, groupOfLocationsExtension=None, routeDestination=None, itineraryExtension=None, predefinedItineraryReference=None, itineraryByReferenceExtension=None, **kwargs_):
        super(ItineraryByReferenceSub, self).__init__(groupOfLocationsExtension, routeDestination, itineraryExtension, predefinedItineraryReference, itineraryByReferenceExtension,  **kwargs_)
supermod.ItineraryByReference.subclass = ItineraryByReferenceSub
# end class ItineraryByReferenceSub


class KilogramsConcentrationValueSub(supermod.KilogramsConcentrationValue):
    def __init__(self, accuracy=None, computationalMethod=None, numberOfIncompleteInputs=None, numberOfInputValuesUsed=None, smoothingFactor=None, standardDeviation=None, supplierCalculatedDataQuality=None, dataError=None, reasonForDataError=None, dataValueExtension=None, kilogramsConcentration=None, kilogramsConcentrationValueExtension=None, **kwargs_):
        super(KilogramsConcentrationValueSub, self).__init__(accuracy, computationalMethod, numberOfIncompleteInputs, numberOfInputValuesUsed, smoothingFactor, standardDeviation, supplierCalculatedDataQuality, dataError, reasonForDataError, dataValueExtension, kilogramsConcentration, kilogramsConcentrationValueExtension,  **kwargs_)
supermod.KilogramsConcentrationValue.subclass = KilogramsConcentrationValueSub
# end class KilogramsConcentrationValueSub


class LengthCharacteristicSub(supermod.LengthCharacteristic):
    def __init__(self, comparisonOperator=None, vehicleLength=None, lengthCharacteristicExtension=None, **kwargs_):
        super(LengthCharacteristicSub, self).__init__(comparisonOperator, vehicleLength, lengthCharacteristicExtension,  **kwargs_)
supermod.LengthCharacteristic.subclass = LengthCharacteristicSub
# end class LengthCharacteristicSub


class LifeCycleManagementSub(supermod.LifeCycleManagement):
    def __init__(self, cancel=None, end=None, lifeCycleManagementExtension=None, **kwargs_):
        super(LifeCycleManagementSub, self).__init__(cancel, end, lifeCycleManagementExtension,  **kwargs_)
supermod.LifeCycleManagement.subclass = LifeCycleManagementSub
# end class LifeCycleManagementSub


class LinearElementSub(supermod.LinearElement):
    def __init__(self, roadName=None, roadNumber=None, linearElementReferenceModel=None, linearElementReferenceModelVersion=None, linearElementNature=None, linearElementExtension=None, extensiontype_=None, **kwargs_):
        super(LinearElementSub, self).__init__(roadName, roadNumber, linearElementReferenceModel, linearElementReferenceModelVersion, linearElementNature, linearElementExtension, extensiontype_,  **kwargs_)
supermod.LinearElement.subclass = LinearElementSub
# end class LinearElementSub


class LinearElementByCodeSub(supermod.LinearElementByCode):
    def __init__(self, roadName=None, roadNumber=None, linearElementReferenceModel=None, linearElementReferenceModelVersion=None, linearElementNature=None, linearElementExtension=None, linearElementIdentifier=None, linearElementByCodeExtension=None, **kwargs_):
        super(LinearElementByCodeSub, self).__init__(roadName, roadNumber, linearElementReferenceModel, linearElementReferenceModelVersion, linearElementNature, linearElementExtension, linearElementIdentifier, linearElementByCodeExtension,  **kwargs_)
supermod.LinearElementByCode.subclass = LinearElementByCodeSub
# end class LinearElementByCodeSub


class LinearElementByPointsSub(supermod.LinearElementByPoints):
    def __init__(self, roadName=None, roadNumber=None, linearElementReferenceModel=None, linearElementReferenceModelVersion=None, linearElementNature=None, linearElementExtension=None, startPointOfLinearElement=None, intermediatePointOnLinearElement=None, endPointOfLinearElement=None, linearElementByPointsExtension=None, **kwargs_):
        super(LinearElementByPointsSub, self).__init__(roadName, roadNumber, linearElementReferenceModel, linearElementReferenceModelVersion, linearElementNature, linearElementExtension, startPointOfLinearElement, intermediatePointOnLinearElement, endPointOfLinearElement, linearElementByPointsExtension,  **kwargs_)
supermod.LinearElementByPoints.subclass = LinearElementByPointsSub
# end class LinearElementByPointsSub


class LinearTrafficViewSub(supermod.LinearTrafficView):
    def __init__(self, id=None, linearPredefinedLocationReference=None, trafficViewRecord=None, linearTrafficViewExtension=None, **kwargs_):
        super(LinearTrafficViewSub, self).__init__(id, linearPredefinedLocationReference, trafficViewRecord, linearTrafficViewExtension,  **kwargs_)
supermod.LinearTrafficView.subclass = LinearTrafficViewSub
# end class LinearTrafficViewSub


class LinearWithinLinearElementSub(supermod.LinearWithinLinearElement):
    def __init__(self, administrativeAreaOfLinearSection=None, directionBoundOnLinearSection=None, directionRelativeOnLinearSection=None, heightGradeOfLinearSection=None, linearElement=None, fromPoint=None, toPoint=None, linearWithinLinearElementExtension=None, **kwargs_):
        super(LinearWithinLinearElementSub, self).__init__(administrativeAreaOfLinearSection, directionBoundOnLinearSection, directionRelativeOnLinearSection, heightGradeOfLinearSection, linearElement, fromPoint, toPoint, linearWithinLinearElementExtension,  **kwargs_)
supermod.LinearWithinLinearElement.subclass = LinearWithinLinearElementSub
# end class LinearWithinLinearElementSub


class LocationSub(supermod.Location):
    def __init__(self, groupOfLocationsExtension=None, externalReferencing=None, locationForDisplay=None, locationExtension=None, extensiontype_=None, **kwargs_):
        super(LocationSub, self).__init__(groupOfLocationsExtension, externalReferencing, locationForDisplay, locationExtension, extensiontype_,  **kwargs_)
supermod.Location.subclass = LocationSub
# end class LocationSub


class LocationByReferenceSub(supermod.LocationByReference):
    def __init__(self, groupOfLocationsExtension=None, externalReferencing=None, locationForDisplay=None, locationExtension=None, predefinedLocationReference=None, locationByReferenceExtension=None, **kwargs_):
        super(LocationByReferenceSub, self).__init__(groupOfLocationsExtension, externalReferencing, locationForDisplay, locationExtension, predefinedLocationReference, locationByReferenceExtension,  **kwargs_)
supermod.LocationByReference.subclass = LocationByReferenceSub
# end class LocationByReferenceSub


class LocationCharacteristicsOverrideSub(supermod.LocationCharacteristicsOverride):
    def __init__(self, measurementLanesOverride=None, reversedFlow=None, locationCharacteristicsOverrideExtension=None, **kwargs_):
        super(LocationCharacteristicsOverrideSub, self).__init__(measurementLanesOverride, reversedFlow, locationCharacteristicsOverrideExtension,  **kwargs_)
supermod.LocationCharacteristicsOverride.subclass = LocationCharacteristicsOverrideSub
# end class LocationCharacteristicsOverrideSub


class MaintenanceVehiclesSub(supermod.MaintenanceVehicles):
    def __init__(self, numberOfMaintenanceVehicles=None, maintenanceVehicleActions=None, maintenanceVehiclesExtension=None, **kwargs_):
        super(MaintenanceVehiclesSub, self).__init__(numberOfMaintenanceVehicles, maintenanceVehicleActions, maintenanceVehiclesExtension,  **kwargs_)
supermod.MaintenanceVehicles.subclass = MaintenanceVehiclesSub
# end class MaintenanceVehiclesSub


class ManagedCauseSub(supermod.ManagedCause):
    def __init__(self, causeExtension=None, managedCause=None, managedCauseExtension=None, **kwargs_):
        super(ManagedCauseSub, self).__init__(causeExtension, managedCause, managedCauseExtension,  **kwargs_)
supermod.ManagedCause.subclass = ManagedCauseSub
# end class ManagedCauseSub


class ManagementSub(supermod.Management):
    def __init__(self, lifeCycleManagement=None, filterExitManagement=None, managementExtension=None, **kwargs_):
        super(ManagementSub, self).__init__(lifeCycleManagement, filterExitManagement, managementExtension,  **kwargs_)
supermod.Management.subclass = ManagementSub
# end class ManagementSub


class MeasuredValueSub(supermod.MeasuredValue):
    def __init__(self, measurementEquipmentTypeUsed=None, locationCharacteristicsOverride=None, measurementEquipmentFault=None, basicData=None, measuredValueExtension=None, **kwargs_):
        super(MeasuredValueSub, self).__init__(measurementEquipmentTypeUsed, locationCharacteristicsOverride, measurementEquipmentFault, basicData, measuredValueExtension,  **kwargs_)
supermod.MeasuredValue.subclass = MeasuredValueSub
# end class MeasuredValueSub


class MeasurementEquipmentFaultSub(supermod.MeasurementEquipmentFault):
    def __init__(self, faultIdentifier=None, faultDescription=None, faultCreationTime=None, faultLastUpdateTime=None, faultSeverity=None, faultExtension=None, measurementEquipmentFault=None, measurementEquipmentFaultExtension=None, **kwargs_):
        super(MeasurementEquipmentFaultSub, self).__init__(faultIdentifier, faultDescription, faultCreationTime, faultLastUpdateTime, faultSeverity, faultExtension, measurementEquipmentFault, measurementEquipmentFaultExtension,  **kwargs_)
supermod.MeasurementEquipmentFault.subclass = MeasurementEquipmentFaultSub
# end class MeasurementEquipmentFaultSub


class MeasurementSiteRecordSub(supermod.MeasurementSiteRecord):
    def __init__(self, id=None, version=None, measurementSiteRecordVersionTime=None, computationMethod=None, measurementEquipmentReference=None, measurementEquipmentTypeUsed=None, measurementSiteName=None, measurementSiteNumberOfLanes=None, measurementSiteIdentification=None, measurementSide=None, measurementSpecificCharacteristics=None, measurementSiteLocation=None, measurementSiteRecordExtension=None, **kwargs_):
        super(MeasurementSiteRecordSub, self).__init__(id, version, measurementSiteRecordVersionTime, computationMethod, measurementEquipmentReference, measurementEquipmentTypeUsed, measurementSiteName, measurementSiteNumberOfLanes, measurementSiteIdentification, measurementSide, measurementSpecificCharacteristics, measurementSiteLocation, measurementSiteRecordExtension,  **kwargs_)
supermod.MeasurementSiteRecord.subclass = MeasurementSiteRecordSub
# end class MeasurementSiteRecordSub


class MeasurementSiteTableSub(supermod.MeasurementSiteTable):
    def __init__(self, id=None, version=None, measurementSiteTableIdentification=None, measurementSiteRecord=None, measurementSiteTableExtension=None, **kwargs_):
        super(MeasurementSiteTableSub, self).__init__(id, version, measurementSiteTableIdentification, measurementSiteRecord, measurementSiteTableExtension,  **kwargs_)
supermod.MeasurementSiteTable.subclass = MeasurementSiteTableSub
# end class MeasurementSiteTableSub


class MeasurementSpecificCharacteristicsSub(supermod.MeasurementSpecificCharacteristics):
    def __init__(self, accuracy=None, period=None, smoothingFactor=None, specificLane=None, specificMeasurementValueType=None, specificVehicleCharacteristics=None, measurementSpecificCharacteristicsExtension=None, **kwargs_):
        super(MeasurementSpecificCharacteristicsSub, self).__init__(accuracy, period, smoothingFactor, specificLane, specificMeasurementValueType, specificVehicleCharacteristics, measurementSpecificCharacteristicsExtension,  **kwargs_)
supermod.MeasurementSpecificCharacteristics.subclass = MeasurementSpecificCharacteristicsSub
# end class MeasurementSpecificCharacteristicsSub


class MicrogramsConcentrationValueSub(supermod.MicrogramsConcentrationValue):
    def __init__(self, accuracy=None, computationalMethod=None, numberOfIncompleteInputs=None, numberOfInputValuesUsed=None, smoothingFactor=None, standardDeviation=None, supplierCalculatedDataQuality=None, dataError=None, reasonForDataError=None, dataValueExtension=None, microgramsConcentration=None, microgramsConcentrationValueExtension=None, **kwargs_):
        super(MicrogramsConcentrationValueSub, self).__init__(accuracy, computationalMethod, numberOfIncompleteInputs, numberOfInputValuesUsed, smoothingFactor, standardDeviation, supplierCalculatedDataQuality, dataError, reasonForDataError, dataValueExtension, microgramsConcentration, microgramsConcentrationValueExtension,  **kwargs_)
supermod.MicrogramsConcentrationValue.subclass = MicrogramsConcentrationValueSub
# end class MicrogramsConcentrationValueSub


class MobilitySub(supermod.Mobility):
    def __init__(self, mobilityType=None, mobilityExtension=None, **kwargs_):
        super(MobilitySub, self).__init__(mobilityType, mobilityExtension,  **kwargs_)
supermod.Mobility.subclass = MobilitySub
# end class MobilitySub


class MultilingualStringSub(supermod.MultilingualString):
    def __init__(self, values=None, **kwargs_):
        super(MultilingualStringSub, self).__init__(values,  **kwargs_)
supermod.MultilingualString.subclass = MultilingualStringSub
# end class MultilingualStringSub


class MultilingualStringValueSub(supermod.MultilingualStringValue):
    def __init__(self, lang=None, valueOf_=None, **kwargs_):
        super(MultilingualStringValueSub, self).__init__(lang, valueOf_,  **kwargs_)
supermod.MultilingualStringValue.subclass = MultilingualStringValueSub
# end class MultilingualStringValueSub


class NetworkLocationSub(supermod.NetworkLocation):
    def __init__(self, groupOfLocationsExtension=None, externalReferencing=None, locationForDisplay=None, locationExtension=None, supplementaryPositionalDescription=None, destination=None, networkLocationExtension=None, extensiontype_=None, **kwargs_):
        super(NetworkLocationSub, self).__init__(groupOfLocationsExtension, externalReferencing, locationForDisplay, locationExtension, supplementaryPositionalDescription, destination, networkLocationExtension, extensiontype_,  **kwargs_)
supermod.NetworkLocation.subclass = NetworkLocationSub
# end class NetworkLocationSub


class NonManagedCauseSub(supermod.NonManagedCause):
    def __init__(self, causeExtension=None, causeDescription=None, causeType=None, nonManagedCauseExtension=None, **kwargs_):
        super(NonManagedCauseSub, self).__init__(causeExtension, causeDescription, causeType, nonManagedCauseExtension,  **kwargs_)
supermod.NonManagedCause.subclass = NonManagedCauseSub
# end class NonManagedCauseSub


class NonOrderedLocationsSub(supermod.NonOrderedLocations):
    def __init__(self, groupOfLocationsExtension=None, nonOrderedLocationsExtension=None, extensiontype_=None, **kwargs_):
        super(NonOrderedLocationsSub, self).__init__(groupOfLocationsExtension, nonOrderedLocationsExtension, extensiontype_,  **kwargs_)
supermod.NonOrderedLocations.subclass = NonOrderedLocationsSub
# end class NonOrderedLocationsSub


class NumberOfAxlesCharacteristicSub(supermod.NumberOfAxlesCharacteristic):
    def __init__(self, comparisonOperator=None, numberOfAxles=None, numberOfAxlesCharacteristicExtension=None, **kwargs_):
        super(NumberOfAxlesCharacteristicSub, self).__init__(comparisonOperator, numberOfAxles, numberOfAxlesCharacteristicExtension,  **kwargs_)
supermod.NumberOfAxlesCharacteristic.subclass = NumberOfAxlesCharacteristicSub
# end class NumberOfAxlesCharacteristicSub


class OffsetDistanceSub(supermod.OffsetDistance):
    def __init__(self, offsetDistance=None, offsetDistanceExtension=None, **kwargs_):
        super(OffsetDistanceSub, self).__init__(offsetDistance, offsetDistanceExtension,  **kwargs_)
supermod.OffsetDistance.subclass = OffsetDistanceSub
# end class OffsetDistanceSub


class OverallPeriodSub(supermod.OverallPeriod):
    def __init__(self, overallStartTime=None, overallEndTime=None, validPeriod=None, exceptionPeriod=None, overallPeriodExtension=None, **kwargs_):
        super(OverallPeriodSub, self).__init__(overallStartTime, overallEndTime, validPeriod, exceptionPeriod, overallPeriodExtension,  **kwargs_)
supermod.OverallPeriod.subclass = OverallPeriodSub
# end class OverallPeriodSub


class PayloadPublicationSub(supermod.PayloadPublication):
    def __init__(self, lang=None, feedDescription=None, feedType=None, publicationTime=None, publicationCreator=None, payloadPublicationExtension=None, extensiontype_=None, **kwargs_):
        super(PayloadPublicationSub, self).__init__(lang, feedDescription, feedType, publicationTime, publicationCreator, payloadPublicationExtension, extensiontype_,  **kwargs_)
supermod.PayloadPublication.subclass = PayloadPublicationSub
# end class PayloadPublicationSub


class PcuFlowValueSub(supermod.PcuFlowValue):
    def __init__(self, accuracy=None, computationalMethod=None, numberOfIncompleteInputs=None, numberOfInputValuesUsed=None, smoothingFactor=None, standardDeviation=None, supplierCalculatedDataQuality=None, dataError=None, reasonForDataError=None, dataValueExtension=None, pcuFlowRate=None, pcuFlowValueExtension=None, **kwargs_):
        super(PcuFlowValueSub, self).__init__(accuracy, computationalMethod, numberOfIncompleteInputs, numberOfInputValuesUsed, smoothingFactor, standardDeviation, supplierCalculatedDataQuality, dataError, reasonForDataError, dataValueExtension, pcuFlowRate, pcuFlowValueExtension,  **kwargs_)
supermod.PcuFlowValue.subclass = PcuFlowValueSub
# end class PcuFlowValueSub


class PercentageDistanceAlongLinearElementSub(supermod.PercentageDistanceAlongLinearElement):
    def __init__(self, distanceAlongLinearElementExtension=None, percentageDistanceAlong=None, percentageDistanceAlongLinearElementExtension=None, **kwargs_):
        super(PercentageDistanceAlongLinearElementSub, self).__init__(distanceAlongLinearElementExtension, percentageDistanceAlong, percentageDistanceAlongLinearElementExtension,  **kwargs_)
supermod.PercentageDistanceAlongLinearElement.subclass = PercentageDistanceAlongLinearElementSub
# end class PercentageDistanceAlongLinearElementSub


class PercentageValueSub(supermod.PercentageValue):
    def __init__(self, accuracy=None, computationalMethod=None, numberOfIncompleteInputs=None, numberOfInputValuesUsed=None, smoothingFactor=None, standardDeviation=None, supplierCalculatedDataQuality=None, dataError=None, reasonForDataError=None, dataValueExtension=None, percentage=None, percentageValueExtension=None, **kwargs_):
        super(PercentageValueSub, self).__init__(accuracy, computationalMethod, numberOfIncompleteInputs, numberOfInputValuesUsed, smoothingFactor, standardDeviation, supplierCalculatedDataQuality, dataError, reasonForDataError, dataValueExtension, percentage, percentageValueExtension,  **kwargs_)
supermod.PercentageValue.subclass = PercentageValueSub
# end class PercentageValueSub


class PeriodSub(supermod.Period):
    def __init__(self, startOfPeriod=None, endOfPeriod=None, periodName=None, recurringTimePeriodOfDay=None, recurringDayWeekMonthPeriod=None, periodExtension=None, **kwargs_):
        super(PeriodSub, self).__init__(startOfPeriod, endOfPeriod, periodName, recurringTimePeriodOfDay, recurringDayWeekMonthPeriod, periodExtension,  **kwargs_)
supermod.Period.subclass = PeriodSub
# end class PeriodSub


class PictogramDisplayAreaSettingsSub(supermod.PictogramDisplayAreaSettings):
    def __init__(self, pictogramLanternsOn=None, pictogramLuminanceOverride=None, pictogramLuminanceLevel=None, pictogramLuminanceLevelName=None, pictogramDisplayAreaSettingsExtension=None, **kwargs_):
        super(PictogramDisplayAreaSettingsSub, self).__init__(pictogramLanternsOn, pictogramLuminanceOverride, pictogramLuminanceLevel, pictogramLuminanceLevelName, pictogramDisplayAreaSettingsExtension,  **kwargs_)
supermod.PictogramDisplayAreaSettings.subclass = PictogramDisplayAreaSettingsSub
# end class PictogramDisplayAreaSettingsSub


class PointSub(supermod.Point):
    def __init__(self, groupOfLocationsExtension=None, externalReferencing=None, locationForDisplay=None, locationExtension=None, supplementaryPositionalDescription=None, destination=None, networkLocationExtension=None, tpegPointLocation=None, alertCPoint=None, pointAlongLinearElement=None, pointByCoordinates=None, pointExtension=None, **kwargs_):
        super(PointSub, self).__init__(groupOfLocationsExtension, externalReferencing, locationForDisplay, locationExtension, supplementaryPositionalDescription, destination, networkLocationExtension, tpegPointLocation, alertCPoint, pointAlongLinearElement, pointByCoordinates, pointExtension,  **kwargs_)
supermod.Point.subclass = PointSub
# end class PointSub


class PointAlongLinearElementSub(supermod.PointAlongLinearElement):
    def __init__(self, administrativeAreaOfPoint=None, directionBoundAtPoint=None, directionRelativeAtPoint=None, heightGradeOfPoint=None, linearElement=None, distanceAlongLinearElement=None, pointAlongLinearElementExtension=None, **kwargs_):
        super(PointAlongLinearElementSub, self).__init__(administrativeAreaOfPoint, directionBoundAtPoint, directionRelativeAtPoint, heightGradeOfPoint, linearElement, distanceAlongLinearElement, pointAlongLinearElementExtension,  **kwargs_)
supermod.PointAlongLinearElement.subclass = PointAlongLinearElementSub
# end class PointAlongLinearElementSub


class PointByCoordinatesSub(supermod.PointByCoordinates):
    def __init__(self, bearing=None, pointCoordinates=None, pointByCoordinatesExtension=None, **kwargs_):
        super(PointByCoordinatesSub, self).__init__(bearing, pointCoordinates, pointByCoordinatesExtension,  **kwargs_)
supermod.PointByCoordinates.subclass = PointByCoordinatesSub
# end class PointByCoordinatesSub


class PointCoordinatesSub(supermod.PointCoordinates):
    def __init__(self, latitude=None, longitude=None, pointCoordinatesExtension=None, **kwargs_):
        super(PointCoordinatesSub, self).__init__(latitude, longitude, pointCoordinatesExtension,  **kwargs_)
supermod.PointCoordinates.subclass = PointCoordinatesSub
# end class PointCoordinatesSub


class PointDestinationSub(supermod.PointDestination):
    def __init__(self, destinationExtension=None, point=None, pointDestinationExtension=None, **kwargs_):
        super(PointDestinationSub, self).__init__(destinationExtension, point, pointDestinationExtension,  **kwargs_)
supermod.PointDestination.subclass = PointDestinationSub
# end class PointDestinationSub


class PollutionSub(supermod.Pollution):
    def __init__(self, pollutantType=None, pollutantConcentration=None, pollutionExtension=None, **kwargs_):
        super(PollutionSub, self).__init__(pollutantType, pollutantConcentration, pollutionExtension,  **kwargs_)
supermod.Pollution.subclass = PollutionSub
# end class PollutionSub


class PrecipitationDetailSub(supermod.PrecipitationDetail):
    def __init__(self, precipitationType=None, precipitationIntensity=None, depositionDepth=None, precipitationDetailExtension=None, **kwargs_):
        super(PrecipitationDetailSub, self).__init__(precipitationType, precipitationIntensity, depositionDepth, precipitationDetailExtension,  **kwargs_)
supermod.PrecipitationDetail.subclass = PrecipitationDetailSub
# end class PrecipitationDetailSub


class PrecipitationIntensityValueSub(supermod.PrecipitationIntensityValue):
    def __init__(self, accuracy=None, computationalMethod=None, numberOfIncompleteInputs=None, numberOfInputValuesUsed=None, smoothingFactor=None, standardDeviation=None, supplierCalculatedDataQuality=None, dataError=None, reasonForDataError=None, dataValueExtension=None, millimetresPerHourIntensity=None, precipitationIntensityValueExtension=None, **kwargs_):
        super(PrecipitationIntensityValueSub, self).__init__(accuracy, computationalMethod, numberOfIncompleteInputs, numberOfInputValuesUsed, smoothingFactor, standardDeviation, supplierCalculatedDataQuality, dataError, reasonForDataError, dataValueExtension, millimetresPerHourIntensity, precipitationIntensityValueExtension,  **kwargs_)
supermod.PrecipitationIntensityValue.subclass = PrecipitationIntensityValueSub
# end class PrecipitationIntensityValueSub


class PredefinedLocationContainerSub(supermod.PredefinedLocationContainer):
    def __init__(self, predefinedLocationContainerExtension=None, extensiontype_=None, **kwargs_):
        super(PredefinedLocationContainerSub, self).__init__(predefinedLocationContainerExtension, extensiontype_,  **kwargs_)
supermod.PredefinedLocationContainer.subclass = PredefinedLocationContainerSub
# end class PredefinedLocationContainerSub


class PredefinedLocationsPublicationSub(supermod.PredefinedLocationsPublication):
    def __init__(self, lang=None, feedDescription=None, feedType=None, publicationTime=None, publicationCreator=None, payloadPublicationExtension=None, headerInformation=None, predefinedLocationContainer=None, predefinedLocationsPublicationExtension=None, **kwargs_):
        super(PredefinedLocationsPublicationSub, self).__init__(lang, feedDescription, feedType, publicationTime, publicationCreator, payloadPublicationExtension, headerInformation, predefinedLocationContainer, predefinedLocationsPublicationExtension,  **kwargs_)
supermod.PredefinedLocationsPublication.subclass = PredefinedLocationsPublicationSub
# end class PredefinedLocationsPublicationSub


class PredefinedNonOrderedLocationGroupSub(supermod.PredefinedNonOrderedLocationGroup):
    def __init__(self, predefinedLocationContainerExtension=None, id=None, version=None, predefinedNonOrderedLocationGroupName=None, predefinedLocation=None, predefinedNonOrderedLocationGroupExtension=None, **kwargs_):
        super(PredefinedNonOrderedLocationGroupSub, self).__init__(predefinedLocationContainerExtension, id, version, predefinedNonOrderedLocationGroupName, predefinedLocation, predefinedNonOrderedLocationGroupExtension,  **kwargs_)
supermod.PredefinedNonOrderedLocationGroup.subclass = PredefinedNonOrderedLocationGroupSub
# end class PredefinedNonOrderedLocationGroupSub


class ReferenceSettingsSub(supermod.ReferenceSettings):
    def __init__(self, predefinedNonOrderedLocationGroupReference=None, trafficStatusDefault=None, referenceSettingsExtension=None, **kwargs_):
        super(ReferenceSettingsSub, self).__init__(predefinedNonOrderedLocationGroupReference, trafficStatusDefault, referenceSettingsExtension,  **kwargs_)
supermod.ReferenceSettings.subclass = ReferenceSettingsSub
# end class ReferenceSettingsSub


class ReferentSub(supermod.Referent):
    def __init__(self, referentIdentifier=None, referentName=None, referentType=None, referentDescription=None, pointCoordinates=None, referentExtension=None, **kwargs_):
        super(ReferentSub, self).__init__(referentIdentifier, referentName, referentType, referentDescription, pointCoordinates, referentExtension,  **kwargs_)
supermod.Referent.subclass = ReferentSub
# end class ReferentSub


class RoadSurfaceConditionMeasurementsSub(supermod.RoadSurfaceConditionMeasurements):
    def __init__(self, roadSurfaceTemperature=None, protectionTemperature=None, deIcingApplicationRate=None, deIcingConcentration=None, depthOfSnow=None, waterFilmThickness=None, roadSurfaceConditionMeasurementsExtension=None, **kwargs_):
        super(RoadSurfaceConditionMeasurementsSub, self).__init__(roadSurfaceTemperature, protectionTemperature, deIcingApplicationRate, deIcingConcentration, depthOfSnow, waterFilmThickness, roadSurfaceConditionMeasurementsExtension,  **kwargs_)
supermod.RoadSurfaceConditionMeasurements.subclass = RoadSurfaceConditionMeasurementsSub
# end class RoadSurfaceConditionMeasurementsSub


class SiteMeasurementsSub(supermod.SiteMeasurements):
    def __init__(self, measurementSiteReference=None, measurementTimeDefault=None, measuredValue=None, siteMeasurementsExtension=None, **kwargs_):
        super(SiteMeasurementsSub, self).__init__(measurementSiteReference, measurementTimeDefault, measuredValue, siteMeasurementsExtension,  **kwargs_)
supermod.SiteMeasurements.subclass = SiteMeasurementsSub
# end class SiteMeasurementsSub


class SituationSub(supermod.Situation):
    def __init__(self, id=None, version=None, overallSeverity=None, relatedSituation=None, situationVersionTime=None, headerInformation=None, situationRecord=None, situationExtension=None, **kwargs_):
        super(SituationSub, self).__init__(id, version, overallSeverity, relatedSituation, situationVersionTime, headerInformation, situationRecord, situationExtension,  **kwargs_)
supermod.Situation.subclass = SituationSub
# end class SituationSub


class SituationPublicationSub(supermod.SituationPublication):
    def __init__(self, lang=None, feedDescription=None, feedType=None, publicationTime=None, publicationCreator=None, payloadPublicationExtension=None, situation=None, situationPublicationExtension=None, **kwargs_):
        super(SituationPublicationSub, self).__init__(lang, feedDescription, feedType, publicationTime, publicationCreator, payloadPublicationExtension, situation, situationPublicationExtension,  **kwargs_)
supermod.SituationPublication.subclass = SituationPublicationSub
# end class SituationPublicationSub


class SituationRecordSub(supermod.SituationRecord):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, extensiontype_=None, **kwargs_):
        super(SituationRecordSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, extensiontype_,  **kwargs_)
supermod.SituationRecord.subclass = SituationRecordSub
# end class SituationRecordSub


class SourceSub(supermod.Source):
    def __init__(self, sourceCountry=None, sourceIdentification=None, sourceName=None, sourceType=None, reliable=None, sourceExtension=None, **kwargs_):
        super(SourceSub, self).__init__(sourceCountry, sourceIdentification, sourceName, sourceType, reliable, sourceExtension,  **kwargs_)
supermod.Source.subclass = SourceSub
# end class SourceSub


class SpeedPercentileSub(supermod.SpeedPercentile):
    def __init__(self, vehiclePercentage=None, speedPercentile=None, speedPercentileExtension=None, **kwargs_):
        super(SpeedPercentileSub, self).__init__(vehiclePercentage, speedPercentile, speedPercentileExtension,  **kwargs_)
supermod.SpeedPercentile.subclass = SpeedPercentileSub
# end class SpeedPercentileSub


class SpeedValueSub(supermod.SpeedValue):
    def __init__(self, accuracy=None, computationalMethod=None, numberOfIncompleteInputs=None, numberOfInputValuesUsed=None, smoothingFactor=None, standardDeviation=None, supplierCalculatedDataQuality=None, dataError=None, reasonForDataError=None, dataValueExtension=None, speed=None, speedValueExtension=None, **kwargs_):
        super(SpeedValueSub, self).__init__(accuracy, computationalMethod, numberOfIncompleteInputs, numberOfInputValuesUsed, smoothingFactor, standardDeviation, supplierCalculatedDataQuality, dataError, reasonForDataError, dataValueExtension, speed, speedValueExtension,  **kwargs_)
supermod.SpeedValue.subclass = SpeedValueSub
# end class SpeedValueSub


class SubjectsSub(supermod.Subjects):
    def __init__(self, subjectTypeOfWorks=None, numberOfSubjects=None, subjectsExtension=None, **kwargs_):
        super(SubjectsSub, self).__init__(subjectTypeOfWorks, numberOfSubjects, subjectsExtension,  **kwargs_)
supermod.Subjects.subclass = SubjectsSub
# end class SubjectsSub


class SubscriptionSub(supermod.Subscription):
    def __init__(self, deleteSubscription=None, deliveryInterval=None, operatingMode=None, subscriptionStartTime=None, subscriptionState=None, subscriptionStopTime=None, updateMethod=None, target=None, filterReference=None, catalogueReference=None, subscriptionExtension=None, **kwargs_):
        super(SubscriptionSub, self).__init__(deleteSubscription, deliveryInterval, operatingMode, subscriptionStartTime, subscriptionState, subscriptionStopTime, updateMethod, target, filterReference, catalogueReference, subscriptionExtension,  **kwargs_)
supermod.Subscription.subclass = SubscriptionSub
# end class SubscriptionSub


class SupplementaryPositionalDescriptionSub(supermod.SupplementaryPositionalDescription):
    def __init__(self, locationPrecision=None, locationDescriptor=None, sequentialRampNumber=None, affectedCarriagewayAndLanes=None, supplementaryPositionalDescriptionExtension=None, **kwargs_):
        super(SupplementaryPositionalDescriptionSub, self).__init__(locationPrecision, locationDescriptor, sequentialRampNumber, affectedCarriagewayAndLanes, supplementaryPositionalDescriptionExtension,  **kwargs_)
supermod.SupplementaryPositionalDescription.subclass = SupplementaryPositionalDescriptionSub
# end class SupplementaryPositionalDescriptionSub


class TargetSub(supermod.Target):
    def __init__(self, address=None, protocol=None, targetExtension=None, **kwargs_):
        super(TargetSub, self).__init__(address, protocol, targetExtension,  **kwargs_)
supermod.Target.subclass = TargetSub
# end class TargetSub


class TemperatureSub(supermod.Temperature):
    def __init__(self, airTemperature=None, dewPointTemperature=None, maximumTemperature=None, minimumTemperature=None, temperatureExtension=None, **kwargs_):
        super(TemperatureSub, self).__init__(airTemperature, dewPointTemperature, maximumTemperature, minimumTemperature, temperatureExtension,  **kwargs_)
supermod.Temperature.subclass = TemperatureSub
# end class TemperatureSub


class TemperatureValueSub(supermod.TemperatureValue):
    def __init__(self, accuracy=None, computationalMethod=None, numberOfIncompleteInputs=None, numberOfInputValuesUsed=None, smoothingFactor=None, standardDeviation=None, supplierCalculatedDataQuality=None, dataError=None, reasonForDataError=None, dataValueExtension=None, temperature=None, temperatureValueExtension=None, **kwargs_):
        super(TemperatureValueSub, self).__init__(accuracy, computationalMethod, numberOfIncompleteInputs, numberOfInputValuesUsed, smoothingFactor, standardDeviation, supplierCalculatedDataQuality, dataError, reasonForDataError, dataValueExtension, temperature, temperatureValueExtension,  **kwargs_)
supermod.TemperatureValue.subclass = TemperatureValueSub
# end class TemperatureValueSub


class TextDisplayAreaSettingsSub(supermod.TextDisplayAreaSettings):
    def __init__(self, textLanternsOn=None, textLuminanceOverride=None, textLuminanceLevel=None, textLuminanceLevelName=None, textDisplayAreaSettingsExtension=None, **kwargs_):
        super(TextDisplayAreaSettingsSub, self).__init__(textLanternsOn, textLuminanceOverride, textLuminanceLevel, textLuminanceLevelName, textDisplayAreaSettingsExtension,  **kwargs_)
supermod.TextDisplayAreaSettings.subclass = TextDisplayAreaSettingsSub
# end class TextDisplayAreaSettingsSub


class TimePeriodOfDaySub(supermod.TimePeriodOfDay):
    def __init__(self, timePeriodOfDayExtension=None, extensiontype_=None, **kwargs_):
        super(TimePeriodOfDaySub, self).__init__(timePeriodOfDayExtension, extensiontype_,  **kwargs_)
supermod.TimePeriodOfDay.subclass = TimePeriodOfDaySub
# end class TimePeriodOfDaySub


class TpegAreaLocationSub(supermod.TpegAreaLocation):
    def __init__(self, tpegAreaLocationType=None, tpegHeight=None, tpegAreaLocationExtension=None, extensiontype_=None, **kwargs_):
        super(TpegAreaLocationSub, self).__init__(tpegAreaLocationType, tpegHeight, tpegAreaLocationExtension, extensiontype_,  **kwargs_)
supermod.TpegAreaLocation.subclass = TpegAreaLocationSub
# end class TpegAreaLocationSub


class TpegDescriptorSub(supermod.TpegDescriptor):
    def __init__(self, descriptor=None, tpegDescriptorExtension=None, extensiontype_=None, **kwargs_):
        super(TpegDescriptorSub, self).__init__(descriptor, tpegDescriptorExtension, extensiontype_,  **kwargs_)
supermod.TpegDescriptor.subclass = TpegDescriptorSub
# end class TpegDescriptorSub


class TpegGeometricAreaSub(supermod.TpegGeometricArea):
    def __init__(self, tpegAreaLocationType=None, tpegHeight=None, tpegAreaLocationExtension=None, radius=None, centrePoint=None, name=None, tpegGeometricAreaExtension=None, **kwargs_):
        super(TpegGeometricAreaSub, self).__init__(tpegAreaLocationType, tpegHeight, tpegAreaLocationExtension, radius, centrePoint, name, tpegGeometricAreaExtension,  **kwargs_)
supermod.TpegGeometricArea.subclass = TpegGeometricAreaSub
# end class TpegGeometricAreaSub


class TpegHeightSub(supermod.TpegHeight):
    def __init__(self, height=None, heightType=None, tpegHeightExtension=None, **kwargs_):
        super(TpegHeightSub, self).__init__(height, heightType, tpegHeightExtension,  **kwargs_)
supermod.TpegHeight.subclass = TpegHeightSub
# end class TpegHeightSub


class TpegLinearLocationSub(supermod.TpegLinearLocation):
    def __init__(self, tpegDirection=None, tpegLinearLocationType=None, to=None, from_=None, tpegLinearLocationExtension=None, **kwargs_):
        super(TpegLinearLocationSub, self).__init__(tpegDirection, tpegLinearLocationType, to, from_, tpegLinearLocationExtension,  **kwargs_)
supermod.TpegLinearLocation.subclass = TpegLinearLocationSub
# end class TpegLinearLocationSub


class TpegNamedOnlyAreaSub(supermod.TpegNamedOnlyArea):
    def __init__(self, tpegAreaLocationType=None, tpegHeight=None, tpegAreaLocationExtension=None, name=None, tpegNamedOnlyAreaExtension=None, **kwargs_):
        super(TpegNamedOnlyAreaSub, self).__init__(tpegAreaLocationType, tpegHeight, tpegAreaLocationExtension, name, tpegNamedOnlyAreaExtension,  **kwargs_)
supermod.TpegNamedOnlyArea.subclass = TpegNamedOnlyAreaSub
# end class TpegNamedOnlyAreaSub


class TpegPointSub(supermod.TpegPoint):
    def __init__(self, tpegPointExtension=None, extensiontype_=None, **kwargs_):
        super(TpegPointSub, self).__init__(tpegPointExtension, extensiontype_,  **kwargs_)
supermod.TpegPoint.subclass = TpegPointSub
# end class TpegPointSub


class TpegPointDescriptorSub(supermod.TpegPointDescriptor):
    def __init__(self, descriptor=None, tpegDescriptorExtension=None, tpegPointDescriptorExtension=None, extensiontype_=None, **kwargs_):
        super(TpegPointDescriptorSub, self).__init__(descriptor, tpegDescriptorExtension, tpegPointDescriptorExtension, extensiontype_,  **kwargs_)
supermod.TpegPointDescriptor.subclass = TpegPointDescriptorSub
# end class TpegPointDescriptorSub


class TpegPointLocationSub(supermod.TpegPointLocation):
    def __init__(self, tpegDirection=None, tpegPointLocationExtension=None, extensiontype_=None, **kwargs_):
        super(TpegPointLocationSub, self).__init__(tpegDirection, tpegPointLocationExtension, extensiontype_,  **kwargs_)
supermod.TpegPointLocation.subclass = TpegPointLocationSub
# end class TpegPointLocationSub


class TpegSimplePointSub(supermod.TpegSimplePoint):
    def __init__(self, tpegDirection=None, tpegPointLocationExtension=None, tpegSimplePointLocationType=None, point=None, tpegSimplePointExtension=None, **kwargs_):
        super(TpegSimplePointSub, self).__init__(tpegDirection, tpegPointLocationExtension, tpegSimplePointLocationType, point, tpegSimplePointExtension,  **kwargs_)
supermod.TpegSimplePoint.subclass = TpegSimplePointSub
# end class TpegSimplePointSub


class TrafficDataSub(supermod.TrafficData):
    def __init__(self, measurementOrCalculatedTimePrecision=None, measurementOrCalculationPeriod=None, measurementOrCalculationTime=None, pertinentLocation=None, basicDataExtension=None, forVehiclesWithCharacteristicsOf=None, trafficDataExtension=None, extensiontype_=None, **kwargs_):
        super(TrafficDataSub, self).__init__(measurementOrCalculatedTimePrecision, measurementOrCalculationPeriod, measurementOrCalculationTime, pertinentLocation, basicDataExtension, forVehiclesWithCharacteristicsOf, trafficDataExtension, extensiontype_,  **kwargs_)
supermod.TrafficData.subclass = TrafficDataSub
# end class TrafficDataSub


class TrafficElementSub(supermod.TrafficElement):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, trafficElementExtension=None, extensiontype_=None, **kwargs_):
        super(TrafficElementSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, trafficElementExtension, extensiontype_,  **kwargs_)
supermod.TrafficElement.subclass = TrafficElementSub
# end class TrafficElementSub


class TrafficFlowSub(supermod.TrafficFlow):
    def __init__(self, measurementOrCalculatedTimePrecision=None, measurementOrCalculationPeriod=None, measurementOrCalculationTime=None, pertinentLocation=None, basicDataExtension=None, forVehiclesWithCharacteristicsOf=None, trafficDataExtension=None, axleFlow=None, pcuFlow=None, percentageLongVehicles=None, vehicleFlow=None, trafficFlowExtension=None, **kwargs_):
        super(TrafficFlowSub, self).__init__(measurementOrCalculatedTimePrecision, measurementOrCalculationPeriod, measurementOrCalculationTime, pertinentLocation, basicDataExtension, forVehiclesWithCharacteristicsOf, trafficDataExtension, axleFlow, pcuFlow, percentageLongVehicles, vehicleFlow, trafficFlowExtension,  **kwargs_)
supermod.TrafficFlow.subclass = TrafficFlowSub
# end class TrafficFlowSub


class TrafficHeadwaySub(supermod.TrafficHeadway):
    def __init__(self, measurementOrCalculatedTimePrecision=None, measurementOrCalculationPeriod=None, measurementOrCalculationTime=None, pertinentLocation=None, basicDataExtension=None, forVehiclesWithCharacteristicsOf=None, trafficDataExtension=None, averageDistanceHeadway=None, averageTimeHeadway=None, trafficHeadwayExtension=None, **kwargs_):
        super(TrafficHeadwaySub, self).__init__(measurementOrCalculatedTimePrecision, measurementOrCalculationPeriod, measurementOrCalculationTime, pertinentLocation, basicDataExtension, forVehiclesWithCharacteristicsOf, trafficDataExtension, averageDistanceHeadway, averageTimeHeadway, trafficHeadwayExtension,  **kwargs_)
supermod.TrafficHeadway.subclass = TrafficHeadwaySub
# end class TrafficHeadwaySub


class TrafficSpeedSub(supermod.TrafficSpeed):
    def __init__(self, measurementOrCalculatedTimePrecision=None, measurementOrCalculationPeriod=None, measurementOrCalculationTime=None, pertinentLocation=None, basicDataExtension=None, forVehiclesWithCharacteristicsOf=None, trafficDataExtension=None, averageVehicleSpeed=None, speedPercentile=None, trafficSpeedExtension=None, **kwargs_):
        super(TrafficSpeedSub, self).__init__(measurementOrCalculatedTimePrecision, measurementOrCalculationPeriod, measurementOrCalculationTime, pertinentLocation, basicDataExtension, forVehiclesWithCharacteristicsOf, trafficDataExtension, averageVehicleSpeed, speedPercentile, trafficSpeedExtension,  **kwargs_)
supermod.TrafficSpeed.subclass = TrafficSpeedSub
# end class TrafficSpeedSub


class TrafficStatusSub(supermod.TrafficStatus):
    def __init__(self, measurementOrCalculatedTimePrecision=None, measurementOrCalculationPeriod=None, measurementOrCalculationTime=None, pertinentLocation=None, basicDataExtension=None, trafficTrendType=None, trafficStatus=None, trafficStatusExtension=None, **kwargs_):
        super(TrafficStatusSub, self).__init__(measurementOrCalculatedTimePrecision, measurementOrCalculationPeriod, measurementOrCalculationTime, pertinentLocation, basicDataExtension, trafficTrendType, trafficStatus, trafficStatusExtension,  **kwargs_)
supermod.TrafficStatus.subclass = TrafficStatusSub
# end class TrafficStatusSub


class TrafficStatusValueSub(supermod.TrafficStatusValue):
    def __init__(self, accuracy=None, computationalMethod=None, numberOfIncompleteInputs=None, numberOfInputValuesUsed=None, smoothingFactor=None, standardDeviation=None, supplierCalculatedDataQuality=None, dataError=None, reasonForDataError=None, dataValueExtension=None, trafficStatusValue=None, trafficStatusValueExtension=None, **kwargs_):
        super(TrafficStatusValueSub, self).__init__(accuracy, computationalMethod, numberOfIncompleteInputs, numberOfInputValuesUsed, smoothingFactor, standardDeviation, supplierCalculatedDataQuality, dataError, reasonForDataError, dataValueExtension, trafficStatusValue, trafficStatusValueExtension,  **kwargs_)
supermod.TrafficStatusValue.subclass = TrafficStatusValueSub
# end class TrafficStatusValueSub


class TrafficViewSub(supermod.TrafficView):
    def __init__(self, id=None, trafficViewTime=None, predefinedNonOrderedLocationGroupReference=None, linearTrafficView=None, trafficViewExtension=None, **kwargs_):
        super(TrafficViewSub, self).__init__(id, trafficViewTime, predefinedNonOrderedLocationGroupReference, linearTrafficView, trafficViewExtension,  **kwargs_)
supermod.TrafficView.subclass = TrafficViewSub
# end class TrafficViewSub


class TrafficViewPublicationSub(supermod.TrafficViewPublication):
    def __init__(self, lang=None, feedDescription=None, feedType=None, publicationTime=None, publicationCreator=None, payloadPublicationExtension=None, headerInformation=None, trafficView=None, trafficViewPublicationExtension=None, **kwargs_):
        super(TrafficViewPublicationSub, self).__init__(lang, feedDescription, feedType, publicationTime, publicationCreator, payloadPublicationExtension, headerInformation, trafficView, trafficViewPublicationExtension,  **kwargs_)
supermod.TrafficViewPublication.subclass = TrafficViewPublicationSub
# end class TrafficViewPublicationSub


class TrafficViewRecordSub(supermod.TrafficViewRecord):
    def __init__(self, id=None, recordSequenceNumber=None, trafficElement=None, operatorAction=None, elaboratedData=None, urlLink=None, trafficViewRecordExtension=None, **kwargs_):
        super(TrafficViewRecordSub, self).__init__(id, recordSequenceNumber, trafficElement, operatorAction, elaboratedData, urlLink, trafficViewRecordExtension,  **kwargs_)
supermod.TrafficViewRecord.subclass = TrafficViewRecordSub
# end class TrafficViewRecordSub


class TravelTimeDataSub(supermod.TravelTimeData):
    def __init__(self, measurementOrCalculatedTimePrecision=None, measurementOrCalculationPeriod=None, measurementOrCalculationTime=None, pertinentLocation=None, basicDataExtension=None, travelTimeTrendType=None, travelTimeType=None, vehicleType=None, travelTime=None, freeFlowTravelTime=None, normallyExpectedTravelTime=None, freeFlowSpeed=None, travelTimeDataExtension=None, **kwargs_):
        super(TravelTimeDataSub, self).__init__(measurementOrCalculatedTimePrecision, measurementOrCalculationPeriod, measurementOrCalculationTime, pertinentLocation, basicDataExtension, travelTimeTrendType, travelTimeType, vehicleType, travelTime, freeFlowTravelTime, normallyExpectedTravelTime, freeFlowSpeed, travelTimeDataExtension,  **kwargs_)
supermod.TravelTimeData.subclass = TravelTimeDataSub
# end class TravelTimeDataSub


class UrlLinkSub(supermod.UrlLink):
    def __init__(self, urlLinkAddress=None, urlLinkDescription=None, urlLinkType=None, urlLinkExtension=None, **kwargs_):
        super(UrlLinkSub, self).__init__(urlLinkAddress, urlLinkDescription, urlLinkType, urlLinkExtension,  **kwargs_)
supermod.UrlLink.subclass = UrlLinkSub
# end class UrlLinkSub


class ValiditySub(supermod.Validity):
    def __init__(self, validityStatus=None, overrunning=None, validityTimeSpecification=None, validityExtension=None, **kwargs_):
        super(ValiditySub, self).__init__(validityStatus, overrunning, validityTimeSpecification, validityExtension,  **kwargs_)
supermod.Validity.subclass = ValiditySub
# end class ValiditySub


class VehicleSub(supermod.Vehicle):
    def __init__(self, vehicleColour=None, vehicleCountryOfOrigin=None, vehicleIdentifier=None, vehicleManufacturer=None, vehicleModel=None, vehicleRegistrationPlateIdentifier=None, vehicleStatus=None, vehicleCharacteristics=None, axleSpacingOnVehicle=None, specificAxleWeight=None, hazardousGoodsAssociatedWithVehicle=None, vehicleExtension=None, **kwargs_):
        super(VehicleSub, self).__init__(vehicleColour, vehicleCountryOfOrigin, vehicleIdentifier, vehicleManufacturer, vehicleModel, vehicleRegistrationPlateIdentifier, vehicleStatus, vehicleCharacteristics, axleSpacingOnVehicle, specificAxleWeight, hazardousGoodsAssociatedWithVehicle, vehicleExtension,  **kwargs_)
supermod.Vehicle.subclass = VehicleSub
# end class VehicleSub


class VehicleCharacteristicsSub(supermod.VehicleCharacteristics):
    def __init__(self, fuelType=None, loadType=None, vehicleEquipment=None, vehicleType=None, vehicleUsage=None, grossWeightCharacteristic=None, heightCharacteristic=None, lengthCharacteristic=None, widthCharacteristic=None, heaviestAxleWeightCharacteristic=None, numberOfAxlesCharacteristic=None, vehicleCharacteristicsExtension=None, **kwargs_):
        super(VehicleCharacteristicsSub, self).__init__(fuelType, loadType, vehicleEquipment, vehicleType, vehicleUsage, grossWeightCharacteristic, heightCharacteristic, lengthCharacteristic, widthCharacteristic, heaviestAxleWeightCharacteristic, numberOfAxlesCharacteristic, vehicleCharacteristicsExtension,  **kwargs_)
supermod.VehicleCharacteristics.subclass = VehicleCharacteristicsSub
# end class VehicleCharacteristicsSub


class VehicleFlowValueSub(supermod.VehicleFlowValue):
    def __init__(self, accuracy=None, computationalMethod=None, numberOfIncompleteInputs=None, numberOfInputValuesUsed=None, smoothingFactor=None, standardDeviation=None, supplierCalculatedDataQuality=None, dataError=None, reasonForDataError=None, dataValueExtension=None, vehicleFlowRate=None, vehicleFlowValueExtension=None, **kwargs_):
        super(VehicleFlowValueSub, self).__init__(accuracy, computationalMethod, numberOfIncompleteInputs, numberOfInputValuesUsed, smoothingFactor, standardDeviation, supplierCalculatedDataQuality, dataError, reasonForDataError, dataValueExtension, vehicleFlowRate, vehicleFlowValueExtension,  **kwargs_)
supermod.VehicleFlowValue.subclass = VehicleFlowValueSub
# end class VehicleFlowValueSub


class VersionedReferenceSub(supermod.VersionedReference):
    def __init__(self, id=None, version=None, extensiontype_=None, **kwargs_):
        super(VersionedReferenceSub, self).__init__(id, version, extensiontype_,  **kwargs_)
supermod.VersionedReference.subclass = VersionedReferenceSub
# end class VersionedReferenceSub


class VisibilitySub(supermod.Visibility):
    def __init__(self, minimumVisibilityDistance=None, visibilityExtension=None, **kwargs_):
        super(VisibilitySub, self).__init__(minimumVisibilityDistance, visibilityExtension,  **kwargs_)
supermod.Visibility.subclass = VisibilitySub
# end class VisibilitySub


class VmsSub(supermod.Vms):
    def __init__(self, vmsWorking=None, vmsMessageSequencingInterval=None, vmsMessage=None, textDisplayAreaSettings=None, pictogramDisplayAreaSettings=None, vmsLocationOverride=None, managedLogicalLocationOverride=None, vmsDynamicCharacteristics=None, vmsFault=None, vmsExtension=None, **kwargs_):
        super(VmsSub, self).__init__(vmsWorking, vmsMessageSequencingInterval, vmsMessage, textDisplayAreaSettings, pictogramDisplayAreaSettings, vmsLocationOverride, managedLogicalLocationOverride, vmsDynamicCharacteristics, vmsFault, vmsExtension,  **kwargs_)
supermod.Vms.subclass = VmsSub
# end class VmsSub


class VmsDynamicCharacteristicsSub(supermod.VmsDynamicCharacteristics):
    def __init__(self, numberOfPictogramDisplayAreas=None, vmsTextDisplayCharacteristics=None, vmsPictogramDisplayCharacteristics=None, vmsDynamicCharacteristicsExtension=None, **kwargs_):
        super(VmsDynamicCharacteristicsSub, self).__init__(numberOfPictogramDisplayAreas, vmsTextDisplayCharacteristics, vmsPictogramDisplayCharacteristics, vmsDynamicCharacteristicsExtension,  **kwargs_)
supermod.VmsDynamicCharacteristics.subclass = VmsDynamicCharacteristicsSub
# end class VmsDynamicCharacteristicsSub


class VmsFaultSub(supermod.VmsFault):
    def __init__(self, faultIdentifier=None, faultDescription=None, faultCreationTime=None, faultLastUpdateTime=None, faultSeverity=None, faultExtension=None, vmsFault=None, vmsFaultExtension=None, **kwargs_):
        super(VmsFaultSub, self).__init__(faultIdentifier, faultDescription, faultCreationTime, faultLastUpdateTime, faultSeverity, faultExtension, vmsFault, vmsFaultExtension,  **kwargs_)
supermod.VmsFault.subclass = VmsFaultSub
# end class VmsFaultSub


class VmsManagedLogicalLocationSub(supermod.VmsManagedLogicalLocation):
    def __init__(self, managedLogicalLocation=None, distanceFromLogicalLocation=None, managedLocation=None, vmsManagedLogicalLocationExtension=None, **kwargs_):
        super(VmsManagedLogicalLocationSub, self).__init__(managedLogicalLocation, distanceFromLogicalLocation, managedLocation, vmsManagedLogicalLocationExtension,  **kwargs_)
supermod.VmsManagedLogicalLocation.subclass = VmsManagedLogicalLocationSub
# end class VmsManagedLogicalLocationSub


class VmsMessageSub(supermod.VmsMessage):
    def __init__(self, associatedManagementOrDiversionPlan=None, messageSetBy=None, setBySystem=None, reasonForSetting=None, codedReasonForSetting=None, vmsMessageInformationType=None, primarySetting=None, mareNostrumCompliant=None, timeLastSet=None, requestedBy=None, situationToWhichMessageIsRelated=None, situationRecordToWhichMessageIsRelated=None, distanceFromSituationRecord=None, textPictogramSequencingInterval=None, textPage=None, vmsPictogramDisplayArea=None, vmsMessageExtension=None, **kwargs_):
        super(VmsMessageSub, self).__init__(associatedManagementOrDiversionPlan, messageSetBy, setBySystem, reasonForSetting, codedReasonForSetting, vmsMessageInformationType, primarySetting, mareNostrumCompliant, timeLastSet, requestedBy, situationToWhichMessageIsRelated, situationRecordToWhichMessageIsRelated, distanceFromSituationRecord, textPictogramSequencingInterval, textPage, vmsPictogramDisplayArea, vmsMessageExtension,  **kwargs_)
supermod.VmsMessage.subclass = VmsMessageSub
# end class VmsMessageSub


class VmsPictogramSub(supermod.VmsPictogram):
    def __init__(self, pictogramDescription=None, pictogramCode=None, pictogramUrl=None, additionalPictogramDescription=None, pictogramFlashing=None, pictogramInInverseColour=None, presenceOfRedTriangle=None, viennaConventionCompliant=None, distanceAttribute=None, heightAttribute=None, lengthAttribute=None, speedAttribute=None, weightAttribute=None, weightPerAxleAttribute=None, widthAttribute=None, vmsSupplementaryPanel=None, vmsPictogramExtension=None, **kwargs_):
        super(VmsPictogramSub, self).__init__(pictogramDescription, pictogramCode, pictogramUrl, additionalPictogramDescription, pictogramFlashing, pictogramInInverseColour, presenceOfRedTriangle, viennaConventionCompliant, distanceAttribute, heightAttribute, lengthAttribute, speedAttribute, weightAttribute, weightPerAxleAttribute, widthAttribute, vmsSupplementaryPanel, vmsPictogramExtension,  **kwargs_)
supermod.VmsPictogram.subclass = VmsPictogramSub
# end class VmsPictogramSub


class VmsPictogramDisplayAreaSub(supermod.VmsPictogramDisplayArea):
    def __init__(self, synchronizedSequencingWithTextPages=None, vmsPictogram=None, vmsPictogramDisplayAreaExtension=None, **kwargs_):
        super(VmsPictogramDisplayAreaSub, self).__init__(synchronizedSequencingWithTextPages, vmsPictogram, vmsPictogramDisplayAreaExtension,  **kwargs_)
supermod.VmsPictogramDisplayArea.subclass = VmsPictogramDisplayAreaSub
# end class VmsPictogramDisplayAreaSub


class VmsPictogramDisplayCharacteristicsSub(supermod.VmsPictogramDisplayCharacteristics):
    def __init__(self, pictogramLanternsPresent=None, pictogramSequencingCapable=None, pictogramPixelsAcross=None, pictogramPixelsDown=None, pictogramDisplayHeight=None, pictogramDisplayWidth=None, pictogramCodeListIdentifier=None, maxPictogramLuminanceLevel=None, pictogramNumberOfColours=None, maxNumberOfSequentialPictograms=None, pictogramPositionAbsolute=None, pictogramPositionX=None, pictogramPositionY=None, pictogramPositionRelativeToText=None, vmsSupplementaryPanelCharacteristics=None, vmsPictogramDisplayCharacteristicsExtension=None, **kwargs_):
        super(VmsPictogramDisplayCharacteristicsSub, self).__init__(pictogramLanternsPresent, pictogramSequencingCapable, pictogramPixelsAcross, pictogramPixelsDown, pictogramDisplayHeight, pictogramDisplayWidth, pictogramCodeListIdentifier, maxPictogramLuminanceLevel, pictogramNumberOfColours, maxNumberOfSequentialPictograms, pictogramPositionAbsolute, pictogramPositionX, pictogramPositionY, pictogramPositionRelativeToText, vmsSupplementaryPanelCharacteristics, vmsPictogramDisplayCharacteristicsExtension,  **kwargs_)
supermod.VmsPictogramDisplayCharacteristics.subclass = VmsPictogramDisplayCharacteristicsSub
# end class VmsPictogramDisplayCharacteristicsSub


class VmsPublicationSub(supermod.VmsPublication):
    def __init__(self, lang=None, feedDescription=None, feedType=None, publicationTime=None, publicationCreator=None, payloadPublicationExtension=None, headerInformation=None, vmsUnit=None, vmsPublicationExtension=None, **kwargs_):
        super(VmsPublicationSub, self).__init__(lang, feedDescription, feedType, publicationTime, publicationCreator, payloadPublicationExtension, headerInformation, vmsUnit, vmsPublicationExtension,  **kwargs_)
supermod.VmsPublication.subclass = VmsPublicationSub
# end class VmsPublicationSub


class VmsRecordSub(supermod.VmsRecord):
    def __init__(self, vmsDescription=None, vmsOwner=None, vmsPhysicalMounting=None, vmsType=None, vmsTypeCode=None, numberOfPictogramDisplayAreas=None, dynamicallyConfigurableDisplayAreas=None, vmsDisplayHeight=None, vmsDisplayWidth=None, vmsHeightAboveRoadway=None, vmsTextDisplayCharacteristics=None, vmsPictogramDisplayCharacteristics=None, vmsLocation=None, vmsManagedLogicalLocation=None, backgroundImageUrl=None, vmsRecordExtension=None, **kwargs_):
        super(VmsRecordSub, self).__init__(vmsDescription, vmsOwner, vmsPhysicalMounting, vmsType, vmsTypeCode, numberOfPictogramDisplayAreas, dynamicallyConfigurableDisplayAreas, vmsDisplayHeight, vmsDisplayWidth, vmsHeightAboveRoadway, vmsTextDisplayCharacteristics, vmsPictogramDisplayCharacteristics, vmsLocation, vmsManagedLogicalLocation, backgroundImageUrl, vmsRecordExtension,  **kwargs_)
supermod.VmsRecord.subclass = VmsRecordSub
# end class VmsRecordSub


class VmsSettingSub(supermod.VmsSetting):
    def __init__(self, vmsSettingExtension=None, extensiontype_=None, **kwargs_):
        super(VmsSettingSub, self).__init__(vmsSettingExtension, extensiontype_,  **kwargs_)
supermod.VmsSetting.subclass = VmsSettingSub
# end class VmsSettingSub


class VmsSupplementaryPanelSub(supermod.VmsSupplementaryPanel):
    def __init__(self, supplementaryMessageDescription=None, vmsSupplementaryPictogram=None, vmsSupplementaryText=None, vmsSupplementaryPanelExtension=None, **kwargs_):
        super(VmsSupplementaryPanelSub, self).__init__(supplementaryMessageDescription, vmsSupplementaryPictogram, vmsSupplementaryText, vmsSupplementaryPanelExtension,  **kwargs_)
supermod.VmsSupplementaryPanel.subclass = VmsSupplementaryPanelSub
# end class VmsSupplementaryPanelSub


class VmsSupplementaryPanelCharacteristicsSub(supermod.VmsSupplementaryPanelCharacteristics):
    def __init__(self, supplementaryPictogramCodeListIdentifier=None, supplementaryPanelPixelsAcross=None, supplementaryPanelPixelsDown=None, supplementaryPanelDisplayHeight=None, supplementaryPanelDisplayWidth=None, supplementaryPanelPositionX=None, supplementaryPanelPositionY=None, relativePositionToPictogramArea=None, vmsSupplementaryPanelCharacteristicsExtension=None, **kwargs_):
        super(VmsSupplementaryPanelCharacteristicsSub, self).__init__(supplementaryPictogramCodeListIdentifier, supplementaryPanelPixelsAcross, supplementaryPanelPixelsDown, supplementaryPanelDisplayHeight, supplementaryPanelDisplayWidth, supplementaryPanelPositionX, supplementaryPanelPositionY, relativePositionToPictogramArea, vmsSupplementaryPanelCharacteristicsExtension,  **kwargs_)
supermod.VmsSupplementaryPanelCharacteristics.subclass = VmsSupplementaryPanelCharacteristicsSub
# end class VmsSupplementaryPanelCharacteristicsSub


class VmsSupplementaryPictogramSub(supermod.VmsSupplementaryPictogram):
    def __init__(self, supplementaryPictogramDescription=None, supplementaryPictogramCode=None, supplementaryPictogramUrl=None, additionalSupplementaryPictogramDescription=None, pictogramFlashing=None, vmsSupplementaryPictogramExtension=None, **kwargs_):
        super(VmsSupplementaryPictogramSub, self).__init__(supplementaryPictogramDescription, supplementaryPictogramCode, supplementaryPictogramUrl, additionalSupplementaryPictogramDescription, pictogramFlashing, vmsSupplementaryPictogramExtension,  **kwargs_)
supermod.VmsSupplementaryPictogram.subclass = VmsSupplementaryPictogramSub
# end class VmsSupplementaryPictogramSub


class VmsTablePublicationSub(supermod.VmsTablePublication):
    def __init__(self, lang=None, feedDescription=None, feedType=None, publicationTime=None, publicationCreator=None, payloadPublicationExtension=None, headerInformation=None, vmsUnitTable=None, vmsTablePublicationExtension=None, **kwargs_):
        super(VmsTablePublicationSub, self).__init__(lang, feedDescription, feedType, publicationTime, publicationCreator, payloadPublicationExtension, headerInformation, vmsUnitTable, vmsTablePublicationExtension,  **kwargs_)
supermod.VmsTablePublication.subclass = VmsTablePublicationSub
# end class VmsTablePublicationSub


class VmsTextSub(supermod.VmsText):
    def __init__(self, vmsLegendCode=None, vmsTextImageUrl=None, vmsTextLine=None, vmsTextExtension=None, **kwargs_):
        super(VmsTextSub, self).__init__(vmsLegendCode, vmsTextImageUrl, vmsTextLine, vmsTextExtension,  **kwargs_)
supermod.VmsText.subclass = VmsTextSub
# end class VmsTextSub


class VmsTextDisplayCharacteristicsSub(supermod.VmsTextDisplayCharacteristics):
    def __init__(self, textLanternsPresent=None, textPageSequencingCapable=None, textPixelsAcross=None, textPixelsDown=None, textDisplayHeight=None, textDisplayWidth=None, maxNumberOfCharacters=None, maxNumberOfRows=None, legendCodeListIdentifier=None, maxFontHeight=None, minFontHeight=None, maxFontWidth=None, minFontWidth=None, maxFontSpacing=None, minFontSpacing=None, maxTextLuminanceLevel=None, maxNumberOfSequentialPages=None, textPositionAbsolute=None, textPositionX=None, textPositionY=None, vmsTextDisplayCharacteristicsExtension=None, **kwargs_):
        super(VmsTextDisplayCharacteristicsSub, self).__init__(textLanternsPresent, textPageSequencingCapable, textPixelsAcross, textPixelsDown, textDisplayHeight, textDisplayWidth, maxNumberOfCharacters, maxNumberOfRows, legendCodeListIdentifier, maxFontHeight, minFontHeight, maxFontWidth, minFontWidth, maxFontSpacing, minFontSpacing, maxTextLuminanceLevel, maxNumberOfSequentialPages, textPositionAbsolute, textPositionX, textPositionY, vmsTextDisplayCharacteristicsExtension,  **kwargs_)
supermod.VmsTextDisplayCharacteristics.subclass = VmsTextDisplayCharacteristicsSub
# end class VmsTextDisplayCharacteristicsSub


class VmsTextLineSub(supermod.VmsTextLine):
    def __init__(self, vmsTextLine=None, vmsTextLineLanguage=None, vmsTextLineColour=None, vmsTextLineFlashing=None, vmsTextLineHtml=None, vmsTextLineExtension=None, **kwargs_):
        super(VmsTextLineSub, self).__init__(vmsTextLine, vmsTextLineLanguage, vmsTextLineColour, vmsTextLineFlashing, vmsTextLineHtml, vmsTextLineExtension,  **kwargs_)
supermod.VmsTextLine.subclass = VmsTextLineSub
# end class VmsTextLineSub


class VmsUnitSub(supermod.VmsUnit):
    def __init__(self, vmsSettingExtension=None, vmsUnitTableReference=None, vmsUnitReference=None, vms=None, vmsUnitFault=None, vmsUnitExtension=None, **kwargs_):
        super(VmsUnitSub, self).__init__(vmsSettingExtension, vmsUnitTableReference, vmsUnitReference, vms, vmsUnitFault, vmsUnitExtension,  **kwargs_)
supermod.VmsUnit.subclass = VmsUnitSub
# end class VmsUnitSub


class VmsUnitFaultSub(supermod.VmsUnitFault):
    def __init__(self, faultIdentifier=None, faultDescription=None, faultCreationTime=None, faultLastUpdateTime=None, faultSeverity=None, faultExtension=None, vmsUnitFault=None, vmsUnitFaultExtension=None, **kwargs_):
        super(VmsUnitFaultSub, self).__init__(faultIdentifier, faultDescription, faultCreationTime, faultLastUpdateTime, faultSeverity, faultExtension, vmsUnitFault, vmsUnitFaultExtension,  **kwargs_)
supermod.VmsUnitFault.subclass = VmsUnitFaultSub
# end class VmsUnitFaultSub


class VmsUnitRecordSub(supermod.VmsUnitRecord):
    def __init__(self, id=None, version=None, numberOfVms=None, vmsUnitIdentifier=None, vmsUnitIPAddress=None, vmsUnitElectronicAddress=None, vmsRecord=None, vmsUnitRecordExtension=None, **kwargs_):
        super(VmsUnitRecordSub, self).__init__(id, version, numberOfVms, vmsUnitIdentifier, vmsUnitIPAddress, vmsUnitElectronicAddress, vmsRecord, vmsUnitRecordExtension,  **kwargs_)
supermod.VmsUnitRecord.subclass = VmsUnitRecordSub
# end class VmsUnitRecordSub


class VmsUnitTableSub(supermod.VmsUnitTable):
    def __init__(self, id=None, version=None, vmsUnitTableIdentification=None, vmsUnitRecord=None, vmsUnitTableExtension=None, **kwargs_):
        super(VmsUnitTableSub, self).__init__(id, version, vmsUnitTableIdentification, vmsUnitRecord, vmsUnitTableExtension,  **kwargs_)
supermod.VmsUnitTable.subclass = VmsUnitTableSub
# end class VmsUnitTableSub


class WeatherDataSub(supermod.WeatherData):
    def __init__(self, measurementOrCalculatedTimePrecision=None, measurementOrCalculationPeriod=None, measurementOrCalculationTime=None, pertinentLocation=None, basicDataExtension=None, weatherDataExtension=None, extensiontype_=None, **kwargs_):
        super(WeatherDataSub, self).__init__(measurementOrCalculatedTimePrecision, measurementOrCalculationPeriod, measurementOrCalculationTime, pertinentLocation, basicDataExtension, weatherDataExtension, extensiontype_,  **kwargs_)
supermod.WeatherData.subclass = WeatherDataSub
# end class WeatherDataSub


class WidthCharacteristicSub(supermod.WidthCharacteristic):
    def __init__(self, comparisonOperator=None, vehicleWidth=None, widthCharacteristicExtension=None, **kwargs_):
        super(WidthCharacteristicSub, self).__init__(comparisonOperator, vehicleWidth, widthCharacteristicExtension,  **kwargs_)
supermod.WidthCharacteristic.subclass = WidthCharacteristicSub
# end class WidthCharacteristicSub


class WindSub(supermod.Wind):
    def __init__(self, windMeasurementHeight=None, windSpeed=None, maximumWindSpeed=None, windDirectionBearing=None, windDirectionCompass=None, windExtension=None, **kwargs_):
        super(WindSub, self).__init__(windMeasurementHeight, windSpeed, maximumWindSpeed, windDirectionBearing, windDirectionCompass, windExtension,  **kwargs_)
supermod.Wind.subclass = WindSub
# end class WindSub


class WindInformationSub(supermod.WindInformation):
    def __init__(self, measurementOrCalculatedTimePrecision=None, measurementOrCalculationPeriod=None, measurementOrCalculationTime=None, pertinentLocation=None, basicDataExtension=None, weatherDataExtension=None, wind=None, windInformationExtension=None, **kwargs_):
        super(WindInformationSub, self).__init__(measurementOrCalculatedTimePrecision, measurementOrCalculationPeriod, measurementOrCalculationTime, pertinentLocation, basicDataExtension, weatherDataExtension, wind, windInformationExtension,  **kwargs_)
supermod.WindInformation.subclass = WindInformationSub
# end class WindInformationSub


class valuesTypeSub(supermod.valuesType):
    def __init__(self, value=None, **kwargs_):
        super(valuesTypeSub, self).__init__(value,  **kwargs_)
supermod.valuesType.subclass = valuesTypeSub
# end class valuesTypeSub


class VisibilityInformationSub(supermod.VisibilityInformation):
    def __init__(self, measurementOrCalculatedTimePrecision=None, measurementOrCalculationPeriod=None, measurementOrCalculationTime=None, pertinentLocation=None, basicDataExtension=None, weatherDataExtension=None, visibility=None, visibilityInformationExtension=None, **kwargs_):
        super(VisibilityInformationSub, self).__init__(measurementOrCalculatedTimePrecision, measurementOrCalculationPeriod, measurementOrCalculationTime, pertinentLocation, basicDataExtension, weatherDataExtension, visibility, visibilityInformationExtension,  **kwargs_)
supermod.VisibilityInformation.subclass = VisibilityInformationSub
# end class VisibilityInformationSub


class TrafficConcentrationSub(supermod.TrafficConcentration):
    def __init__(self, measurementOrCalculatedTimePrecision=None, measurementOrCalculationPeriod=None, measurementOrCalculationTime=None, pertinentLocation=None, basicDataExtension=None, forVehiclesWithCharacteristicsOf=None, trafficDataExtension=None, concentration=None, occupancy=None, trafficConcentrationExtension=None, **kwargs_):
        super(TrafficConcentrationSub, self).__init__(measurementOrCalculatedTimePrecision, measurementOrCalculationPeriod, measurementOrCalculationTime, pertinentLocation, basicDataExtension, forVehiclesWithCharacteristicsOf, trafficDataExtension, concentration, occupancy, trafficConcentrationExtension,  **kwargs_)
supermod.TrafficConcentration.subclass = TrafficConcentrationSub
# end class TrafficConcentrationSub


class TpegOtherPointDescriptorSub(supermod.TpegOtherPointDescriptor):
    def __init__(self, descriptor=None, tpegDescriptorExtension=None, tpegPointDescriptorExtension=None, tpegOtherPointDescriptorType=None, tpegOtherPointDescriptorExtension=None, **kwargs_):
        super(TpegOtherPointDescriptorSub, self).__init__(descriptor, tpegDescriptorExtension, tpegPointDescriptorExtension, tpegOtherPointDescriptorType, tpegOtherPointDescriptorExtension,  **kwargs_)
supermod.TpegOtherPointDescriptor.subclass = TpegOtherPointDescriptorSub
# end class TpegOtherPointDescriptorSub


class TpegNonJunctionPointSub(supermod.TpegNonJunctionPoint):
    def __init__(self, tpegPointExtension=None, pointCoordinates=None, name=None, tpegNonJunctionPointExtension=None, **kwargs_):
        super(TpegNonJunctionPointSub, self).__init__(tpegPointExtension, pointCoordinates, name, tpegNonJunctionPointExtension,  **kwargs_)
supermod.TpegNonJunctionPoint.subclass = TpegNonJunctionPointSub
# end class TpegNonJunctionPointSub


class TpegJunctionPointDescriptorSub(supermod.TpegJunctionPointDescriptor):
    def __init__(self, descriptor=None, tpegDescriptorExtension=None, tpegPointDescriptorExtension=None, tpegJunctionPointDescriptorType=None, tpegJunctionPointDescriptorExtension=None, **kwargs_):
        super(TpegJunctionPointDescriptorSub, self).__init__(descriptor, tpegDescriptorExtension, tpegPointDescriptorExtension, tpegJunctionPointDescriptorType, tpegJunctionPointDescriptorExtension,  **kwargs_)
supermod.TpegJunctionPointDescriptor.subclass = TpegJunctionPointDescriptorSub
# end class TpegJunctionPointDescriptorSub


class TpegJunctionSub(supermod.TpegJunction):
    def __init__(self, tpegPointExtension=None, pointCoordinates=None, name=None, ilc=None, otherName=None, tpegJunctionExtension=None, **kwargs_):
        super(TpegJunctionSub, self).__init__(tpegPointExtension, pointCoordinates, name, ilc, otherName, tpegJunctionExtension,  **kwargs_)
supermod.TpegJunction.subclass = TpegJunctionSub
# end class TpegJunctionSub


class TpegIlcPointDescriptorSub(supermod.TpegIlcPointDescriptor):
    def __init__(self, descriptor=None, tpegDescriptorExtension=None, tpegPointDescriptorExtension=None, tpegIlcPointDescriptorType=None, tpegIlcPointDescriptorExtension=None, **kwargs_):
        super(TpegIlcPointDescriptorSub, self).__init__(descriptor, tpegDescriptorExtension, tpegPointDescriptorExtension, tpegIlcPointDescriptorType, tpegIlcPointDescriptorExtension,  **kwargs_)
supermod.TpegIlcPointDescriptor.subclass = TpegIlcPointDescriptorSub
# end class TpegIlcPointDescriptorSub


class TpegFramedPointSub(supermod.TpegFramedPoint):
    def __init__(self, tpegDirection=None, tpegPointLocationExtension=None, tpegFramedPointLocationType=None, framedPoint=None, to=None, from_=None, tpegFramedPointExtension=None, **kwargs_):
        super(TpegFramedPointSub, self).__init__(tpegDirection, tpegPointLocationExtension, tpegFramedPointLocationType, framedPoint, to, from_, tpegFramedPointExtension,  **kwargs_)
supermod.TpegFramedPoint.subclass = TpegFramedPointSub
# end class TpegFramedPointSub


class TpegAreaDescriptorSub(supermod.TpegAreaDescriptor):
    def __init__(self, descriptor=None, tpegDescriptorExtension=None, tpegAreaDescriptorType=None, tpegAreaDescriptorExtension=None, **kwargs_):
        super(TpegAreaDescriptorSub, self).__init__(descriptor, tpegDescriptorExtension, tpegAreaDescriptorType, tpegAreaDescriptorExtension,  **kwargs_)
supermod.TpegAreaDescriptor.subclass = TpegAreaDescriptorSub
# end class TpegAreaDescriptorSub


class TimePeriodByHourSub(supermod.TimePeriodByHour):
    def __init__(self, timePeriodOfDayExtension=None, startTimeOfPeriod=None, endTimeOfPeriod=None, timePeriodByHourExtension=None, **kwargs_):
        super(TimePeriodByHourSub, self).__init__(timePeriodOfDayExtension, startTimeOfPeriod, endTimeOfPeriod, timePeriodByHourExtension,  **kwargs_)
supermod.TimePeriodByHour.subclass = TimePeriodByHourSub
# end class TimePeriodByHourSub


class TemperatureInformationSub(supermod.TemperatureInformation):
    def __init__(self, measurementOrCalculatedTimePrecision=None, measurementOrCalculationPeriod=None, measurementOrCalculationTime=None, pertinentLocation=None, basicDataExtension=None, weatherDataExtension=None, temperature=None, temperatureInformationExtension=None, **kwargs_):
        super(TemperatureInformationSub, self).__init__(measurementOrCalculatedTimePrecision, measurementOrCalculationPeriod, measurementOrCalculationTime, pertinentLocation, basicDataExtension, weatherDataExtension, temperature, temperatureInformationExtension,  **kwargs_)
supermod.TemperatureInformation.subclass = TemperatureInformationSub
# end class TemperatureInformationSub


class RoadSurfaceConditionInformationSub(supermod.RoadSurfaceConditionInformation):
    def __init__(self, measurementOrCalculatedTimePrecision=None, measurementOrCalculationPeriod=None, measurementOrCalculationTime=None, pertinentLocation=None, basicDataExtension=None, weatherDataExtension=None, weatherRelatedRoadConditionType=None, roadSurfaceConditionMeasurements=None, roadSurfaceConditionInformationExtension=None, **kwargs_):
        super(RoadSurfaceConditionInformationSub, self).__init__(measurementOrCalculatedTimePrecision, measurementOrCalculationPeriod, measurementOrCalculationTime, pertinentLocation, basicDataExtension, weatherDataExtension, weatherRelatedRoadConditionType, roadSurfaceConditionMeasurements, roadSurfaceConditionInformationExtension,  **kwargs_)
supermod.RoadSurfaceConditionInformation.subclass = RoadSurfaceConditionInformationSub
# end class RoadSurfaceConditionInformationSub


class PredefinedLocationSub(supermod.PredefinedLocation):
    def __init__(self, predefinedLocationContainerExtension=None, id=None, version=None, predefinedLocationName=None, location=None, predefinedLocationExtension=None, **kwargs_):
        super(PredefinedLocationSub, self).__init__(predefinedLocationContainerExtension, id, version, predefinedLocationName, location, predefinedLocationExtension,  **kwargs_)
supermod.PredefinedLocation.subclass = PredefinedLocationSub
# end class PredefinedLocationSub


class PredefinedItinerarySub(supermod.PredefinedItinerary):
    def __init__(self, predefinedLocationContainerExtension=None, id=None, version=None, predefinedItineraryName=None, predefinedLocation=None, predefinedItineraryExtension=None, **kwargs_):
        super(PredefinedItinerarySub, self).__init__(predefinedLocationContainerExtension, id, version, predefinedItineraryName, predefinedLocation, predefinedItineraryExtension,  **kwargs_)
supermod.PredefinedItinerary.subclass = PredefinedItinerarySub
# end class PredefinedItinerarySub


class PrecipitationInformationSub(supermod.PrecipitationInformation):
    def __init__(self, measurementOrCalculatedTimePrecision=None, measurementOrCalculationPeriod=None, measurementOrCalculationTime=None, pertinentLocation=None, basicDataExtension=None, weatherDataExtension=None, noPrecipitation=None, precipitationDetail=None, precipitationInformationExtension=None, **kwargs_):
        super(PrecipitationInformationSub, self).__init__(measurementOrCalculatedTimePrecision, measurementOrCalculationPeriod, measurementOrCalculationTime, pertinentLocation, basicDataExtension, weatherDataExtension, noPrecipitation, precipitationDetail, precipitationInformationExtension,  **kwargs_)
supermod.PrecipitationInformation.subclass = PrecipitationInformationSub
# end class PrecipitationInformationSub


class PollutionInformationSub(supermod.PollutionInformation):
    def __init__(self, measurementOrCalculatedTimePrecision=None, measurementOrCalculationPeriod=None, measurementOrCalculationTime=None, pertinentLocation=None, basicDataExtension=None, weatherDataExtension=None, pollution=None, pollutionInformationExtension=None, **kwargs_):
        super(PollutionInformationSub, self).__init__(measurementOrCalculatedTimePrecision, measurementOrCalculationPeriod, measurementOrCalculationTime, pertinentLocation, basicDataExtension, weatherDataExtension, pollution, pollutionInformationExtension,  **kwargs_)
supermod.PollutionInformation.subclass = PollutionInformationSub
# end class PollutionInformationSub


class OperatorActionSub(supermod.OperatorAction):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, actionOrigin=None, actionPlanIdentifier=None, operatorActionStatus=None, operatorActionExtension=None, extensiontype_=None, **kwargs_):
        super(OperatorActionSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, actionOrigin, actionPlanIdentifier, operatorActionStatus, operatorActionExtension, extensiontype_,  **kwargs_)
supermod.OperatorAction.subclass = OperatorActionSub
# end class OperatorActionSub


class ObstructionSub(supermod.Obstruction):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, trafficElementExtension=None, numberOfObstructions=None, mobilityOfObstruction=None, obstructionExtension=None, extensiontype_=None, **kwargs_):
        super(ObstructionSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, trafficElementExtension, numberOfObstructions, mobilityOfObstruction, obstructionExtension, extensiontype_,  **kwargs_)
supermod.Obstruction.subclass = ObstructionSub
# end class ObstructionSub


class NonRoadEventInformationSub(supermod.NonRoadEventInformation):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, nonRoadEventInformationExtension=None, extensiontype_=None, **kwargs_):
        super(NonRoadEventInformationSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, nonRoadEventInformationExtension, extensiontype_,  **kwargs_)
supermod.NonRoadEventInformation.subclass = NonRoadEventInformationSub
# end class NonRoadEventInformationSub


class NonOrderedLocationGroupByReferenceSub(supermod.NonOrderedLocationGroupByReference):
    def __init__(self, groupOfLocationsExtension=None, nonOrderedLocationsExtension=None, predefinedNonOrderedLocationGroupReference=None, nonOrderedLocationGroupByReferenceExtension=None, **kwargs_):
        super(NonOrderedLocationGroupByReferenceSub, self).__init__(groupOfLocationsExtension, nonOrderedLocationsExtension, predefinedNonOrderedLocationGroupReference, nonOrderedLocationGroupByReferenceExtension,  **kwargs_)
supermod.NonOrderedLocationGroupByReference.subclass = NonOrderedLocationGroupByReferenceSub
# end class NonOrderedLocationGroupByReferenceSub


class NonOrderedLocationGroupByListSub(supermod.NonOrderedLocationGroupByList):
    def __init__(self, groupOfLocationsExtension=None, nonOrderedLocationsExtension=None, locationContainedInGroup=None, nonOrderedLocationGroupByListExtension=None, **kwargs_):
        super(NonOrderedLocationGroupByListSub, self).__init__(groupOfLocationsExtension, nonOrderedLocationsExtension, locationContainedInGroup, nonOrderedLocationGroupByListExtension,  **kwargs_)
supermod.NonOrderedLocationGroupByList.subclass = NonOrderedLocationGroupByListSub
# end class NonOrderedLocationGroupByListSub


class NetworkManagementSub(supermod.NetworkManagement):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, actionOrigin=None, actionPlanIdentifier=None, operatorActionStatus=None, operatorActionExtension=None, complianceOption=None, applicableForTrafficDirection=None, applicableForTrafficType=None, placesAtWhichApplicable=None, automaticallyInitiated=None, forVehiclesWithCharacteristicsOf=None, networkManagementExtension=None, extensiontype_=None, **kwargs_):
        super(NetworkManagementSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, actionOrigin, actionPlanIdentifier, operatorActionStatus, operatorActionExtension, complianceOption, applicableForTrafficDirection, applicableForTrafficType, placesAtWhichApplicable, automaticallyInitiated, forVehiclesWithCharacteristicsOf, networkManagementExtension, extensiontype_,  **kwargs_)
supermod.NetworkManagement.subclass = NetworkManagementSub
# end class NetworkManagementSub


class MeasurementSiteTablePublicationSub(supermod.MeasurementSiteTablePublication):
    def __init__(self, lang=None, feedDescription=None, feedType=None, publicationTime=None, publicationCreator=None, payloadPublicationExtension=None, headerInformation=None, measurementSiteTable=None, measurementSiteTablePublicationExtension=None, **kwargs_):
        super(MeasurementSiteTablePublicationSub, self).__init__(lang, feedDescription, feedType, publicationTime, publicationCreator, payloadPublicationExtension, headerInformation, measurementSiteTable, measurementSiteTablePublicationExtension,  **kwargs_)
supermod.MeasurementSiteTablePublication.subclass = MeasurementSiteTablePublicationSub
# end class MeasurementSiteTablePublicationSub


class MeasuredDataPublicationSub(supermod.MeasuredDataPublication):
    def __init__(self, lang=None, feedDescription=None, feedType=None, publicationTime=None, publicationCreator=None, payloadPublicationExtension=None, measurementSiteTableReference=None, headerInformation=None, siteMeasurements=None, measuredDataPublicationExtension=None, **kwargs_):
        super(MeasuredDataPublicationSub, self).__init__(lang, feedDescription, feedType, publicationTime, publicationCreator, payloadPublicationExtension, measurementSiteTableReference, headerInformation, siteMeasurements, measuredDataPublicationExtension,  **kwargs_)
supermod.MeasuredDataPublication.subclass = MeasuredDataPublicationSub
# end class MeasuredDataPublicationSub


class LinearSub(supermod.Linear):
    def __init__(self, groupOfLocationsExtension=None, externalReferencing=None, locationForDisplay=None, locationExtension=None, supplementaryPositionalDescription=None, destination=None, networkLocationExtension=None, tpegLinearLocation=None, alertCLinear=None, linearWithinLinearElement=None, linearExtension=None, **kwargs_):
        super(LinearSub, self).__init__(groupOfLocationsExtension, externalReferencing, locationForDisplay, locationExtension, supplementaryPositionalDescription, destination, networkLocationExtension, tpegLinearLocation, alertCLinear, linearWithinLinearElement, linearExtension,  **kwargs_)
supermod.Linear.subclass = LinearSub
# end class LinearSub


class InfrastructureDamageObstructionSub(supermod.InfrastructureDamageObstruction):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, trafficElementExtension=None, numberOfObstructions=None, mobilityOfObstruction=None, obstructionExtension=None, infrastructureDamageType=None, infrastructureDamageObstructionExtension=None, **kwargs_):
        super(InfrastructureDamageObstructionSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, trafficElementExtension, numberOfObstructions, mobilityOfObstruction, obstructionExtension, infrastructureDamageType, infrastructureDamageObstructionExtension,  **kwargs_)
supermod.InfrastructureDamageObstruction.subclass = InfrastructureDamageObstructionSub
# end class InfrastructureDamageObstructionSub


class IndividualVehicleDataValuesSub(supermod.IndividualVehicleDataValues):
    def __init__(self, measurementOrCalculatedTimePrecision=None, measurementOrCalculationPeriod=None, measurementOrCalculationTime=None, pertinentLocation=None, basicDataExtension=None, forVehiclesWithCharacteristicsOf=None, trafficDataExtension=None, individualVehicleSpeed=None, arrivalTime=None, exitTime=None, passageDurationTime=None, presenceDurationTime=None, timeGap=None, timeHeadway=None, distanceGap=None, distanceHeadway=None, individualVehicleDataValuesExtension=None, **kwargs_):
        super(IndividualVehicleDataValuesSub, self).__init__(measurementOrCalculatedTimePrecision, measurementOrCalculationPeriod, measurementOrCalculationTime, pertinentLocation, basicDataExtension, forVehiclesWithCharacteristicsOf, trafficDataExtension, individualVehicleSpeed, arrivalTime, exitTime, passageDurationTime, presenceDurationTime, timeGap, timeHeadway, distanceGap, distanceHeadway, individualVehicleDataValuesExtension,  **kwargs_)
supermod.IndividualVehicleDataValues.subclass = IndividualVehicleDataValuesSub
# end class IndividualVehicleDataValuesSub


class HumidityInformationSub(supermod.HumidityInformation):
    def __init__(self, measurementOrCalculatedTimePrecision=None, measurementOrCalculationPeriod=None, measurementOrCalculationTime=None, pertinentLocation=None, basicDataExtension=None, weatherDataExtension=None, humidity=None, humidityInformationExtension=None, **kwargs_):
        super(HumidityInformationSub, self).__init__(measurementOrCalculatedTimePrecision, measurementOrCalculationPeriod, measurementOrCalculationTime, pertinentLocation, basicDataExtension, weatherDataExtension, humidity, humidityInformationExtension,  **kwargs_)
supermod.HumidityInformation.subclass = HumidityInformationSub
# end class HumidityInformationSub


class GenericSituationRecordSub(supermod.GenericSituationRecord):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, genericSituationRecordName=None, genericSituationRecordExtension=None, **kwargs_):
        super(GenericSituationRecordSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, genericSituationRecordName, genericSituationRecordExtension,  **kwargs_)
supermod.GenericSituationRecord.subclass = GenericSituationRecordSub
# end class GenericSituationRecordSub


class GenericPublicationSub(supermod.GenericPublication):
    def __init__(self, lang=None, feedDescription=None, feedType=None, publicationTime=None, publicationCreator=None, payloadPublicationExtension=None, genericPublicationName=None, genericPublicationExtension=None, **kwargs_):
        super(GenericPublicationSub, self).__init__(lang, feedDescription, feedType, publicationTime, publicationCreator, payloadPublicationExtension, genericPublicationName, genericPublicationExtension,  **kwargs_)
supermod.GenericPublication.subclass = GenericPublicationSub
# end class GenericPublicationSub


class GeneralObstructionSub(supermod.GeneralObstruction):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, trafficElementExtension=None, numberOfObstructions=None, mobilityOfObstruction=None, obstructionExtension=None, obstructionType=None, groupOfPeopleInvolved=None, generalObstructionExtension=None, **kwargs_):
        super(GeneralObstructionSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, trafficElementExtension, numberOfObstructions, mobilityOfObstruction, obstructionExtension, obstructionType, groupOfPeopleInvolved, generalObstructionExtension,  **kwargs_)
supermod.GeneralObstruction.subclass = GeneralObstructionSub
# end class GeneralObstructionSub


class GeneralNetworkManagementSub(supermod.GeneralNetworkManagement):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, actionOrigin=None, actionPlanIdentifier=None, operatorActionStatus=None, operatorActionExtension=None, complianceOption=None, applicableForTrafficDirection=None, applicableForTrafficType=None, placesAtWhichApplicable=None, automaticallyInitiated=None, forVehiclesWithCharacteristicsOf=None, networkManagementExtension=None, generalNetworkManagementType=None, trafficManuallyDirectedBy=None, generalNetworkManagementExtension=None, **kwargs_):
        super(GeneralNetworkManagementSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, actionOrigin, actionPlanIdentifier, operatorActionStatus, operatorActionExtension, complianceOption, applicableForTrafficDirection, applicableForTrafficType, placesAtWhichApplicable, automaticallyInitiated, forVehiclesWithCharacteristicsOf, networkManagementExtension, generalNetworkManagementType, trafficManuallyDirectedBy, generalNetworkManagementExtension,  **kwargs_)
supermod.GeneralNetworkManagement.subclass = GeneralNetworkManagementSub
# end class GeneralNetworkManagementSub


class GeneralInstructionOrMessageToRoadUsersSub(supermod.GeneralInstructionOrMessageToRoadUsers):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, actionOrigin=None, actionPlanIdentifier=None, operatorActionStatus=None, operatorActionExtension=None, complianceOption=None, applicableForTrafficDirection=None, applicableForTrafficType=None, placesAtWhichApplicable=None, automaticallyInitiated=None, forVehiclesWithCharacteristicsOf=None, networkManagementExtension=None, generalInstructionToRoadUsersType=None, generalMessageToRoadUsers=None, generalInstructionOrMessageToRoadUsersExtension=None, **kwargs_):
        super(GeneralInstructionOrMessageToRoadUsersSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, actionOrigin, actionPlanIdentifier, operatorActionStatus, operatorActionExtension, complianceOption, applicableForTrafficDirection, applicableForTrafficType, placesAtWhichApplicable, automaticallyInitiated, forVehiclesWithCharacteristicsOf, networkManagementExtension, generalInstructionToRoadUsersType, generalMessageToRoadUsers, generalInstructionOrMessageToRoadUsersExtension,  **kwargs_)
supermod.GeneralInstructionOrMessageToRoadUsers.subclass = GeneralInstructionOrMessageToRoadUsersSub
# end class GeneralInstructionOrMessageToRoadUsersSub


class EquipmentOrSystemFaultSub(supermod.EquipmentOrSystemFault):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, trafficElementExtension=None, equipmentOrSystemFaultType=None, faultyEquipmentOrSystemType=None, equipmentOrSystemFaultExtension=None, **kwargs_):
        super(EquipmentOrSystemFaultSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, trafficElementExtension, equipmentOrSystemFaultType, faultyEquipmentOrSystemType, equipmentOrSystemFaultExtension,  **kwargs_)
supermod.EquipmentOrSystemFault.subclass = EquipmentOrSystemFaultSub
# end class EquipmentOrSystemFaultSub


class EnvironmentalObstructionSub(supermod.EnvironmentalObstruction):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, trafficElementExtension=None, numberOfObstructions=None, mobilityOfObstruction=None, obstructionExtension=None, depth=None, environmentalObstructionType=None, environmentalObstructionExtension=None, **kwargs_):
        super(EnvironmentalObstructionSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, trafficElementExtension, numberOfObstructions, mobilityOfObstruction, obstructionExtension, depth, environmentalObstructionType, environmentalObstructionExtension,  **kwargs_)
supermod.EnvironmentalObstruction.subclass = EnvironmentalObstructionSub
# end class EnvironmentalObstructionSub


class ElaboratedDataPublicationSub(supermod.ElaboratedDataPublication):
    def __init__(self, lang=None, feedDescription=None, feedType=None, publicationTime=None, publicationCreator=None, payloadPublicationExtension=None, forecastDefault=None, periodDefault=None, timeDefault=None, headerInformation=None, referenceSettings=None, elaboratedData=None, elaboratedDataPublicationExtension=None, **kwargs_):
        super(ElaboratedDataPublicationSub, self).__init__(lang, feedDescription, feedType, publicationTime, publicationCreator, payloadPublicationExtension, forecastDefault, periodDefault, timeDefault, headerInformation, referenceSettings, elaboratedData, elaboratedDataPublicationExtension,  **kwargs_)
supermod.ElaboratedDataPublication.subclass = ElaboratedDataPublicationSub
# end class ElaboratedDataPublicationSub


class ElaboratedDataFaultSub(supermod.ElaboratedDataFault):
    def __init__(self, faultIdentifier=None, faultDescription=None, faultCreationTime=None, faultLastUpdateTime=None, faultSeverity=None, faultExtension=None, elaboratedDataFault=None, elaboratedDataFaultExtension=None, **kwargs_):
        super(ElaboratedDataFaultSub, self).__init__(faultIdentifier, faultDescription, faultCreationTime, faultLastUpdateTime, faultSeverity, faultExtension, elaboratedDataFault, elaboratedDataFaultExtension,  **kwargs_)
supermod.ElaboratedDataFault.subclass = ElaboratedDataFaultSub
# end class ElaboratedDataFaultSub


class ConditionsSub(supermod.Conditions):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, trafficElementExtension=None, drivingConditionType=None, conditionsExtension=None, extensiontype_=None, **kwargs_):
        super(ConditionsSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, trafficElementExtension, drivingConditionType, conditionsExtension, extensiontype_,  **kwargs_)
supermod.Conditions.subclass = ConditionsSub
# end class ConditionsSub


class ConcentrationOfVehiclesValueSub(supermod.ConcentrationOfVehiclesValue):
    def __init__(self, accuracy=None, computationalMethod=None, numberOfIncompleteInputs=None, numberOfInputValuesUsed=None, smoothingFactor=None, standardDeviation=None, supplierCalculatedDataQuality=None, dataError=None, reasonForDataError=None, dataValueExtension=None, concentrationOfVehicles=None, concentrationOfVehiclesValueExtension=None, **kwargs_):
        super(ConcentrationOfVehiclesValueSub, self).__init__(accuracy, computationalMethod, numberOfIncompleteInputs, numberOfInputValuesUsed, smoothingFactor, standardDeviation, supplierCalculatedDataQuality, dataError, reasonForDataError, dataValueExtension, concentrationOfVehicles, concentrationOfVehiclesValueExtension,  **kwargs_)
supermod.ConcentrationOfVehiclesValue.subclass = ConcentrationOfVehiclesValueSub
# end class ConcentrationOfVehiclesValueSub


class CarParksSub(supermod.CarParks):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, nonRoadEventInformationExtension=None, carParkConfiguration=None, carParkIdentity=None, carParkOccupancy=None, carParkStatus=None, exitRate=None, fillRate=None, numberOfVacantParkingSpaces=None, occupiedSpaces=None, queuingTime=None, totalCapacity=None, carParksExtension=None, **kwargs_):
        super(CarParksSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, nonRoadEventInformationExtension, carParkConfiguration, carParkIdentity, carParkOccupancy, carParkStatus, exitRate, fillRate, numberOfVacantParkingSpaces, occupiedSpaces, queuingTime, totalCapacity, carParksExtension,  **kwargs_)
supermod.CarParks.subclass = CarParksSub
# end class CarParksSub


class AxleFlowValueSub(supermod.AxleFlowValue):
    def __init__(self, accuracy=None, computationalMethod=None, numberOfIncompleteInputs=None, numberOfInputValuesUsed=None, smoothingFactor=None, standardDeviation=None, supplierCalculatedDataQuality=None, dataError=None, reasonForDataError=None, dataValueExtension=None, axleFlowRate=None, axleFlowValueExtension=None, **kwargs_):
        super(AxleFlowValueSub, self).__init__(accuracy, computationalMethod, numberOfIncompleteInputs, numberOfInputValuesUsed, smoothingFactor, standardDeviation, supplierCalculatedDataQuality, dataError, reasonForDataError, dataValueExtension, axleFlowRate, axleFlowValueExtension,  **kwargs_)
supermod.AxleFlowValue.subclass = AxleFlowValueSub
# end class AxleFlowValueSub


class AreaDestinationSub(supermod.AreaDestination):
    def __init__(self, destinationExtension=None, area=None, areaDestinationExtension=None, **kwargs_):
        super(AreaDestinationSub, self).__init__(destinationExtension, area, areaDestinationExtension,  **kwargs_)
supermod.AreaDestination.subclass = AreaDestinationSub
# end class AreaDestinationSub


class AreaSub(supermod.Area):
    def __init__(self, groupOfLocationsExtension=None, externalReferencing=None, locationForDisplay=None, locationExtension=None, alertCArea=None, tpegAreaLocation=None, areaExtension=None, **kwargs_):
        super(AreaSub, self).__init__(groupOfLocationsExtension, externalReferencing, locationForDisplay, locationExtension, alertCArea, tpegAreaLocation, areaExtension,  **kwargs_)
supermod.Area.subclass = AreaSub
# end class AreaSub


class ApplicationRateValueSub(supermod.ApplicationRateValue):
    def __init__(self, accuracy=None, computationalMethod=None, numberOfIncompleteInputs=None, numberOfInputValuesUsed=None, smoothingFactor=None, standardDeviation=None, supplierCalculatedDataQuality=None, dataError=None, reasonForDataError=None, dataValueExtension=None, applicationRate=None, applicationRateValueExtension=None, **kwargs_):
        super(ApplicationRateValueSub, self).__init__(accuracy, computationalMethod, numberOfIncompleteInputs, numberOfInputValuesUsed, smoothingFactor, standardDeviation, supplierCalculatedDataQuality, dataError, reasonForDataError, dataValueExtension, applicationRate, applicationRateValueExtension,  **kwargs_)
supermod.ApplicationRateValue.subclass = ApplicationRateValueSub
# end class ApplicationRateValueSub


class AnimalPresenceObstructionSub(supermod.AnimalPresenceObstruction):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, trafficElementExtension=None, numberOfObstructions=None, mobilityOfObstruction=None, obstructionExtension=None, alive=None, animalPresenceType=None, animalPresenceObstructionExtension=None, **kwargs_):
        super(AnimalPresenceObstructionSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, trafficElementExtension, numberOfObstructions, mobilityOfObstruction, obstructionExtension, alive, animalPresenceType, animalPresenceObstructionExtension,  **kwargs_)
supermod.AnimalPresenceObstruction.subclass = AnimalPresenceObstructionSub
# end class AnimalPresenceObstructionSub


class AlertCMethod4PointSub(supermod.AlertCMethod4Point):
    def __init__(self, alertCLocationCountryCode=None, alertCLocationTableNumber=None, alertCLocationTableVersion=None, alertCPointExtension=None, alertCDirection=None, alertCMethod4PrimaryPointLocation=None, alertCMethod4PointExtension=None, **kwargs_):
        super(AlertCMethod4PointSub, self).__init__(alertCLocationCountryCode, alertCLocationTableNumber, alertCLocationTableVersion, alertCPointExtension, alertCDirection, alertCMethod4PrimaryPointLocation, alertCMethod4PointExtension,  **kwargs_)
supermod.AlertCMethod4Point.subclass = AlertCMethod4PointSub
# end class AlertCMethod4PointSub


class AlertCMethod2PointSub(supermod.AlertCMethod2Point):
    def __init__(self, alertCLocationCountryCode=None, alertCLocationTableNumber=None, alertCLocationTableVersion=None, alertCPointExtension=None, alertCDirection=None, alertCMethod2PrimaryPointLocation=None, alertCMethod2PointExtension=None, **kwargs_):
        super(AlertCMethod2PointSub, self).__init__(alertCLocationCountryCode, alertCLocationTableNumber, alertCLocationTableVersion, alertCPointExtension, alertCDirection, alertCMethod2PrimaryPointLocation, alertCMethod2PointExtension,  **kwargs_)
supermod.AlertCMethod2Point.subclass = AlertCMethod2PointSub
# end class AlertCMethod2PointSub


class ActivitySub(supermod.Activity):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, trafficElementExtension=None, mobilityOfActivity=None, activityExtension=None, extensiontype_=None, **kwargs_):
        super(ActivitySub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, trafficElementExtension, mobilityOfActivity, activityExtension, extensiontype_,  **kwargs_)
supermod.Activity.subclass = ActivitySub
# end class ActivitySub


class AccidentSub(supermod.Accident):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, trafficElementExtension=None, accidentCause=None, accidentType=None, totalNumberOfPeopleInvolved=None, totalNumberOfVehiclesInvolved=None, vehicleInvolved=None, groupOfVehiclesInvolved=None, groupOfPeopleInvolved=None, accidentExtension=None, **kwargs_):
        super(AccidentSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, trafficElementExtension, accidentCause, accidentType, totalNumberOfPeopleInvolved, totalNumberOfVehiclesInvolved, vehicleInvolved, groupOfVehiclesInvolved, groupOfPeopleInvolved, accidentExtension,  **kwargs_)
supermod.Accident.subclass = AccidentSub
# end class AccidentSub


class AbnormalTrafficSub(supermod.AbnormalTraffic):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, trafficElementExtension=None, abnormalTrafficType=None, numberOfVehiclesWaiting=None, queueLength=None, relativeTrafficFlow=None, trafficFlowCharacteristics=None, trafficTrendType=None, abnormalTrafficExtension=None, **kwargs_):
        super(AbnormalTrafficSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, trafficElementExtension, abnormalTrafficType, numberOfVehiclesWaiting, queueLength, relativeTrafficFlow, trafficFlowCharacteristics, trafficTrendType, abnormalTrafficExtension,  **kwargs_)
supermod.AbnormalTraffic.subclass = AbnormalTrafficSub
# end class AbnormalTrafficSub


class _VmsUnitTableVersionedReferenceSub(supermod._VmsUnitTableVersionedReference):
    def __init__(self, id=None, version=None, targetClass='VmsUnitTable', **kwargs_):
        super(_VmsUnitTableVersionedReferenceSub, self).__init__(id, version, targetClass,  **kwargs_)
supermod._VmsUnitTableVersionedReference.subclass = _VmsUnitTableVersionedReferenceSub
# end class _VmsUnitTableVersionedReferenceSub


class _VmsUnitRecordVersionedReferenceSub(supermod._VmsUnitRecordVersionedReference):
    def __init__(self, id=None, version=None, targetClass='VmsUnitRecord', **kwargs_):
        super(_VmsUnitRecordVersionedReferenceSub, self).__init__(id, version, targetClass,  **kwargs_)
supermod._VmsUnitRecordVersionedReference.subclass = _VmsUnitRecordVersionedReferenceSub
# end class _VmsUnitRecordVersionedReferenceSub


class _SituationVersionedReferenceSub(supermod._SituationVersionedReference):
    def __init__(self, id=None, version=None, targetClass='Situation', **kwargs_):
        super(_SituationVersionedReferenceSub, self).__init__(id, version, targetClass,  **kwargs_)
supermod._SituationVersionedReference.subclass = _SituationVersionedReferenceSub
# end class _SituationVersionedReferenceSub


class _SituationRecordVersionedReferenceSub(supermod._SituationRecordVersionedReference):
    def __init__(self, id=None, version=None, targetClass='SituationRecord', **kwargs_):
        super(_SituationRecordVersionedReferenceSub, self).__init__(id, version, targetClass,  **kwargs_)
supermod._SituationRecordVersionedReference.subclass = _SituationRecordVersionedReferenceSub
# end class _SituationRecordVersionedReferenceSub


class _PredefinedNonOrderedLocationGroupVersionedReferenceSub(supermod._PredefinedNonOrderedLocationGroupVersionedReference):
    def __init__(self, id=None, version=None, targetClass='PredefinedNonOrderedLocationGroup', **kwargs_):
        super(_PredefinedNonOrderedLocationGroupVersionedReferenceSub, self).__init__(id, version, targetClass,  **kwargs_)
supermod._PredefinedNonOrderedLocationGroupVersionedReference.subclass = _PredefinedNonOrderedLocationGroupVersionedReferenceSub
# end class _PredefinedNonOrderedLocationGroupVersionedReferenceSub


class _PredefinedLocationVersionedReferenceSub(supermod._PredefinedLocationVersionedReference):
    def __init__(self, id=None, version=None, targetClass='PredefinedLocation', **kwargs_):
        super(_PredefinedLocationVersionedReferenceSub, self).__init__(id, version, targetClass,  **kwargs_)
supermod._PredefinedLocationVersionedReference.subclass = _PredefinedLocationVersionedReferenceSub
# end class _PredefinedLocationVersionedReferenceSub


class _PredefinedItineraryVersionedReferenceSub(supermod._PredefinedItineraryVersionedReference):
    def __init__(self, id=None, version=None, targetClass='PredefinedItinerary', **kwargs_):
        super(_PredefinedItineraryVersionedReferenceSub, self).__init__(id, version, targetClass,  **kwargs_)
supermod._PredefinedItineraryVersionedReference.subclass = _PredefinedItineraryVersionedReferenceSub
# end class _PredefinedItineraryVersionedReferenceSub


class _MeasurementSiteTableVersionedReferenceSub(supermod._MeasurementSiteTableVersionedReference):
    def __init__(self, id=None, version=None, targetClass='MeasurementSiteTable', **kwargs_):
        super(_MeasurementSiteTableVersionedReferenceSub, self).__init__(id, version, targetClass,  **kwargs_)
supermod._MeasurementSiteTableVersionedReference.subclass = _MeasurementSiteTableVersionedReferenceSub
# end class _MeasurementSiteTableVersionedReferenceSub


class _MeasurementSiteRecordVersionedReferenceSub(supermod._MeasurementSiteRecordVersionedReference):
    def __init__(self, id=None, version=None, targetClass='MeasurementSiteRecord', **kwargs_):
        super(_MeasurementSiteRecordVersionedReferenceSub, self).__init__(id, version, targetClass,  **kwargs_)
supermod._MeasurementSiteRecordVersionedReference.subclass = _MeasurementSiteRecordVersionedReferenceSub
# end class _MeasurementSiteRecordVersionedReferenceSub


class WinterDrivingManagementSub(supermod.WinterDrivingManagement):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, actionOrigin=None, actionPlanIdentifier=None, operatorActionStatus=None, operatorActionExtension=None, complianceOption=None, applicableForTrafficDirection=None, applicableForTrafficType=None, placesAtWhichApplicable=None, automaticallyInitiated=None, forVehiclesWithCharacteristicsOf=None, networkManagementExtension=None, winterEquipmentManagementType=None, winterDrivingManagementExtension=None, **kwargs_):
        super(WinterDrivingManagementSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, actionOrigin, actionPlanIdentifier, operatorActionStatus, operatorActionExtension, complianceOption, applicableForTrafficDirection, applicableForTrafficType, placesAtWhichApplicable, automaticallyInitiated, forVehiclesWithCharacteristicsOf, networkManagementExtension, winterEquipmentManagementType, winterDrivingManagementExtension,  **kwargs_)
supermod.WinterDrivingManagement.subclass = WinterDrivingManagementSub
# end class WinterDrivingManagementSub


class VehicleObstructionSub(supermod.VehicleObstruction):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, trafficElementExtension=None, numberOfObstructions=None, mobilityOfObstruction=None, obstructionExtension=None, vehicleObstructionType=None, obstructingVehicle=None, vehicleObstructionExtension=None, **kwargs_):
        super(VehicleObstructionSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, trafficElementExtension, numberOfObstructions, mobilityOfObstruction, obstructionExtension, vehicleObstructionType, obstructingVehicle, vehicleObstructionExtension,  **kwargs_)
supermod.VehicleObstruction.subclass = VehicleObstructionSub
# end class VehicleObstructionSub


class TransitInformationSub(supermod.TransitInformation):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, nonRoadEventInformationExtension=None, journeyDestination=None, journeyOrigin=None, journeyReference=None, transitServiceInformation=None, transitServiceType=None, scheduledDepartureTime=None, transitInformationExtension=None, **kwargs_):
        super(TransitInformationSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, nonRoadEventInformationExtension, journeyDestination, journeyOrigin, journeyReference, transitServiceInformation, transitServiceType, scheduledDepartureTime, transitInformationExtension,  **kwargs_)
supermod.TransitInformation.subclass = TransitInformationSub
# end class TransitInformationSub


class SpeedManagementSub(supermod.SpeedManagement):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, actionOrigin=None, actionPlanIdentifier=None, operatorActionStatus=None, operatorActionExtension=None, complianceOption=None, applicableForTrafficDirection=None, applicableForTrafficType=None, placesAtWhichApplicable=None, automaticallyInitiated=None, forVehiclesWithCharacteristicsOf=None, networkManagementExtension=None, speedManagementType=None, temporarySpeedLimit=None, speedManagementExtension=None, **kwargs_):
        super(SpeedManagementSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, actionOrigin, actionPlanIdentifier, operatorActionStatus, operatorActionExtension, complianceOption, applicableForTrafficDirection, applicableForTrafficType, placesAtWhichApplicable, automaticallyInitiated, forVehiclesWithCharacteristicsOf, networkManagementExtension, speedManagementType, temporarySpeedLimit, speedManagementExtension,  **kwargs_)
supermod.SpeedManagement.subclass = SpeedManagementSub
# end class SpeedManagementSub


class SignSettingSub(supermod.SignSetting):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, actionOrigin=None, actionPlanIdentifier=None, operatorActionStatus=None, operatorActionExtension=None, vmsSetting=None, signSettingExtension=None, **kwargs_):
        super(SignSettingSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, actionOrigin, actionPlanIdentifier, operatorActionStatus, operatorActionExtension, vmsSetting, signSettingExtension,  **kwargs_)
supermod.SignSetting.subclass = SignSettingSub
# end class SignSettingSub


class RoadworksSub(supermod.Roadworks):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, actionOrigin=None, actionPlanIdentifier=None, operatorActionStatus=None, operatorActionExtension=None, roadworksDuration=None, roadworksScale=None, underTraffic=None, urgentRoadworks=None, mobility=None, subjects=None, maintenanceVehicles=None, roadworksExtension=None, extensiontype_=None, **kwargs_):
        super(RoadworksSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, actionOrigin, actionPlanIdentifier, operatorActionStatus, operatorActionExtension, roadworksDuration, roadworksScale, underTraffic, urgentRoadworks, mobility, subjects, maintenanceVehicles, roadworksExtension, extensiontype_,  **kwargs_)
supermod.Roadworks.subclass = RoadworksSub
# end class RoadworksSub


class RoadsideServiceDisruptionSub(supermod.RoadsideServiceDisruption):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, nonRoadEventInformationExtension=None, roadsideServiceDisruptionType=None, roadsideServiceDisruptionExtension=None, **kwargs_):
        super(RoadsideServiceDisruptionSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, nonRoadEventInformationExtension, roadsideServiceDisruptionType, roadsideServiceDisruptionExtension,  **kwargs_)
supermod.RoadsideServiceDisruption.subclass = RoadsideServiceDisruptionSub
# end class RoadsideServiceDisruptionSub


class RoadsideAssistanceSub(supermod.RoadsideAssistance):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, actionOrigin=None, actionPlanIdentifier=None, operatorActionStatus=None, operatorActionExtension=None, roadsideAssistanceType=None, roadsideAssistanceExtension=None, **kwargs_):
        super(RoadsideAssistanceSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, actionOrigin, actionPlanIdentifier, operatorActionStatus, operatorActionExtension, roadsideAssistanceType, roadsideAssistanceExtension,  **kwargs_)
supermod.RoadsideAssistance.subclass = RoadsideAssistanceSub
# end class RoadsideAssistanceSub


class RoadOrCarriagewayOrLaneManagementSub(supermod.RoadOrCarriagewayOrLaneManagement):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, actionOrigin=None, actionPlanIdentifier=None, operatorActionStatus=None, operatorActionExtension=None, complianceOption=None, applicableForTrafficDirection=None, applicableForTrafficType=None, placesAtWhichApplicable=None, automaticallyInitiated=None, forVehiclesWithCharacteristicsOf=None, networkManagementExtension=None, roadOrCarriagewayOrLaneManagementType=None, minimumCarOccupancy=None, roadOrCarriagewayOrLaneManagementExtension=None, **kwargs_):
        super(RoadOrCarriagewayOrLaneManagementSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, actionOrigin, actionPlanIdentifier, operatorActionStatus, operatorActionExtension, complianceOption, applicableForTrafficDirection, applicableForTrafficType, placesAtWhichApplicable, automaticallyInitiated, forVehiclesWithCharacteristicsOf, networkManagementExtension, roadOrCarriagewayOrLaneManagementType, minimumCarOccupancy, roadOrCarriagewayOrLaneManagementExtension,  **kwargs_)
supermod.RoadOrCarriagewayOrLaneManagement.subclass = RoadOrCarriagewayOrLaneManagementSub
# end class RoadOrCarriagewayOrLaneManagementSub


class RoadOperatorServiceDisruptionSub(supermod.RoadOperatorServiceDisruption):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, nonRoadEventInformationExtension=None, roadOperatorServiceDisruptionType=None, roadOperatorServiceDisruptionExtension=None, **kwargs_):
        super(RoadOperatorServiceDisruptionSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, nonRoadEventInformationExtension, roadOperatorServiceDisruptionType, roadOperatorServiceDisruptionExtension,  **kwargs_)
supermod.RoadOperatorServiceDisruption.subclass = RoadOperatorServiceDisruptionSub
# end class RoadOperatorServiceDisruptionSub


class RoadConditionsSub(supermod.RoadConditions):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, trafficElementExtension=None, drivingConditionType=None, conditionsExtension=None, roadConditionsExtension=None, extensiontype_=None, **kwargs_):
        super(RoadConditionsSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, trafficElementExtension, drivingConditionType, conditionsExtension, roadConditionsExtension, extensiontype_,  **kwargs_)
supermod.RoadConditions.subclass = RoadConditionsSub
# end class RoadConditionsSub


class ReroutingManagementSub(supermod.ReroutingManagement):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, actionOrigin=None, actionPlanIdentifier=None, operatorActionStatus=None, operatorActionExtension=None, complianceOption=None, applicableForTrafficDirection=None, applicableForTrafficType=None, placesAtWhichApplicable=None, automaticallyInitiated=None, forVehiclesWithCharacteristicsOf=None, networkManagementExtension=None, reroutingManagementType=None, reroutingItineraryDescription=None, signedRerouting=None, entry=None, exit=None, roadOrJunctionNumber=None, alternativeRoute=None, reroutingManagementExtension=None, **kwargs_):
        super(ReroutingManagementSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, actionOrigin, actionPlanIdentifier, operatorActionStatus, operatorActionExtension, complianceOption, applicableForTrafficDirection, applicableForTrafficType, placesAtWhichApplicable, automaticallyInitiated, forVehiclesWithCharacteristicsOf, networkManagementExtension, reroutingManagementType, reroutingItineraryDescription, signedRerouting, entry, exit, roadOrJunctionNumber, alternativeRoute, reroutingManagementExtension,  **kwargs_)
supermod.ReroutingManagement.subclass = ReroutingManagementSub
# end class ReroutingManagementSub


class PublicEventSub(supermod.PublicEvent):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, trafficElementExtension=None, mobilityOfActivity=None, activityExtension=None, publicEventType=None, publicEventExtension=None, **kwargs_):
        super(PublicEventSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, trafficElementExtension, mobilityOfActivity, activityExtension, publicEventType, publicEventExtension,  **kwargs_)
supermod.PublicEvent.subclass = PublicEventSub
# end class PublicEventSub


class PoorEnvironmentConditionsSub(supermod.PoorEnvironmentConditions):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, trafficElementExtension=None, drivingConditionType=None, conditionsExtension=None, poorEnvironmentType=None, precipitationDetail=None, visibility=None, pollution=None, temperature=None, wind=None, humidity=None, poorEnvironmentConditionsExtension=None, **kwargs_):
        super(PoorEnvironmentConditionsSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, trafficElementExtension, drivingConditionType, conditionsExtension, poorEnvironmentType, precipitationDetail, visibility, pollution, temperature, wind, humidity, poorEnvironmentConditionsExtension,  **kwargs_)
supermod.PoorEnvironmentConditions.subclass = PoorEnvironmentConditionsSub
# end class PoorEnvironmentConditionsSub


class NonWeatherRelatedRoadConditionsSub(supermod.NonWeatherRelatedRoadConditions):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, trafficElementExtension=None, drivingConditionType=None, conditionsExtension=None, roadConditionsExtension=None, nonWeatherRelatedRoadConditionType=None, nonWeatherRelatedRoadConditionsExtension=None, **kwargs_):
        super(NonWeatherRelatedRoadConditionsSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, trafficElementExtension, drivingConditionType, conditionsExtension, roadConditionsExtension, nonWeatherRelatedRoadConditionType, nonWeatherRelatedRoadConditionsExtension,  **kwargs_)
supermod.NonWeatherRelatedRoadConditions.subclass = NonWeatherRelatedRoadConditionsSub
# end class NonWeatherRelatedRoadConditionsSub


class MaintenanceWorksSub(supermod.MaintenanceWorks):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, actionOrigin=None, actionPlanIdentifier=None, operatorActionStatus=None, operatorActionExtension=None, roadworksDuration=None, roadworksScale=None, underTraffic=None, urgentRoadworks=None, mobility=None, subjects=None, maintenanceVehicles=None, roadworksExtension=None, roadMaintenanceType=None, maintenanceWorksExtension=None, **kwargs_):
        super(MaintenanceWorksSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, actionOrigin, actionPlanIdentifier, operatorActionStatus, operatorActionExtension, roadworksDuration, roadworksScale, underTraffic, urgentRoadworks, mobility, subjects, maintenanceVehicles, roadworksExtension, roadMaintenanceType, maintenanceWorksExtension,  **kwargs_)
supermod.MaintenanceWorks.subclass = MaintenanceWorksSub
# end class MaintenanceWorksSub


class DisturbanceActivitySub(supermod.DisturbanceActivity):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, trafficElementExtension=None, mobilityOfActivity=None, activityExtension=None, disturbanceActivityType=None, disturbanceActivityExtension=None, **kwargs_):
        super(DisturbanceActivitySub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, trafficElementExtension, mobilityOfActivity, activityExtension, disturbanceActivityType, disturbanceActivityExtension,  **kwargs_)
supermod.DisturbanceActivity.subclass = DisturbanceActivitySub
# end class DisturbanceActivitySub


class ConstructionWorksSub(supermod.ConstructionWorks):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, actionOrigin=None, actionPlanIdentifier=None, operatorActionStatus=None, operatorActionExtension=None, roadworksDuration=None, roadworksScale=None, underTraffic=None, urgentRoadworks=None, mobility=None, subjects=None, maintenanceVehicles=None, roadworksExtension=None, constructionWorkType=None, constructionWorksExtension=None, **kwargs_):
        super(ConstructionWorksSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, actionOrigin, actionPlanIdentifier, operatorActionStatus, operatorActionExtension, roadworksDuration, roadworksScale, underTraffic, urgentRoadworks, mobility, subjects, maintenanceVehicles, roadworksExtension, constructionWorkType, constructionWorksExtension,  **kwargs_)
supermod.ConstructionWorks.subclass = ConstructionWorksSub
# end class ConstructionWorksSub


class AuthorityOperationSub(supermod.AuthorityOperation):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, trafficElementExtension=None, mobilityOfActivity=None, activityExtension=None, authorityOperationType=None, authorityOperationExtension=None, **kwargs_):
        super(AuthorityOperationSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, trafficElementExtension, mobilityOfActivity, activityExtension, authorityOperationType, authorityOperationExtension,  **kwargs_)
supermod.AuthorityOperation.subclass = AuthorityOperationSub
# end class AuthorityOperationSub


class WeatherRelatedRoadConditionsSub(supermod.WeatherRelatedRoadConditions):
    def __init__(self, id=None, version=None, situationRecordCreationReference=None, situationRecordCreationTime=None, situationRecordObservationTime=None, situationRecordVersionTime=None, situationRecordFirstSupplierVersionTime=None, confidentialityOverride=None, probabilityOfOccurrence=None, severity=None, source=None, validity=None, impact=None, cause=None, generalPublicComment=None, nonGeneralPublicComment=None, urlLink=None, groupOfLocations=None, management=None, situationRecordExtension=None, trafficElementExtension=None, drivingConditionType=None, conditionsExtension=None, roadConditionsExtension=None, weatherRelatedRoadConditionType=None, roadSurfaceConditionMeasurements=None, weatherRelatedRoadConditionsExtension=None, **kwargs_):
        super(WeatherRelatedRoadConditionsSub, self).__init__(id, version, situationRecordCreationReference, situationRecordCreationTime, situationRecordObservationTime, situationRecordVersionTime, situationRecordFirstSupplierVersionTime, confidentialityOverride, probabilityOfOccurrence, severity, source, validity, impact, cause, generalPublicComment, nonGeneralPublicComment, urlLink, groupOfLocations, management, situationRecordExtension, trafficElementExtension, drivingConditionType, conditionsExtension, roadConditionsExtension, weatherRelatedRoadConditionType, roadSurfaceConditionMeasurements, weatherRelatedRoadConditionsExtension,  **kwargs_)
supermod.WeatherRelatedRoadConditions.subclass = WeatherRelatedRoadConditionsSub
# end class WeatherRelatedRoadConditionsSub


def get_root_tag(node):
    tag = supermod.Tag_pattern_.match(node.tag).groups()[-1]
    rootClass = None
    rootClass = supermod.GDSClassesMapping.get(tag)
    if rootClass is None and hasattr(supermod, tag):
        rootClass = getattr(supermod, tag)
    return tag, rootClass


def parse(inFilename, silence=False):
    parser = None
    doc = parsexml_(inFilename, parser)
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = '_ExtensionType'
        rootClass = supermod._ExtensionType
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    # Enable Python to collect the space used by the DOM.
    doc = None
    if not silence:
        sys.stdout.write('<?xml version="1.0" ?>\n')
        rootObj.export(
            sys.stdout, 0, name_=rootTag,
            namespacedef_='xmlns:D2LogicalModel="http://datex2.eu/schema/2/2_0"',
            pretty_print=True)
    return rootObj


def parseEtree(inFilename, silence=False):
    parser = None
    doc = parsexml_(inFilename, parser)
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = '_ExtensionType'
        rootClass = supermod._ExtensionType
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    # Enable Python to collect the space used by the DOM.
    doc = None
    mapping = {}
    rootElement = rootObj.to_etree(None, name_=rootTag, mapping_=mapping)
    reverse_mapping = rootObj.gds_reverse_node_mapping(mapping)
    if not silence:
        content = etree_.tostring(
            rootElement, pretty_print=True,
            xml_declaration=True, encoding="utf-8")
        sys.stdout.write(content)
        sys.stdout.write('\n')
    return rootObj, rootElement, mapping, reverse_mapping


def parseString(inString, silence=False):
    if sys.version_info.major == 2:
        from StringIO import StringIO
    else:
        from io import BytesIO as StringIO
    parser = None
    doc = parsexml_(StringIO(inString), parser)
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = '_ExtensionType'
        rootClass = supermod._ExtensionType
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    # Enable Python to collect the space used by the DOM.
    doc = None
    if not silence:
        sys.stdout.write('<?xml version="1.0" ?>\n')
        rootObj.export(
            sys.stdout, 0, name_=rootTag,
            namespacedef_='xmlns:D2LogicalModel="http://datex2.eu/schema/2/2_0"')
    return rootObj


def parseLiteral(inFilename, silence=False):
    parser = None
    doc = parsexml_(inFilename, parser)
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = '_ExtensionType'
        rootClass = supermod._ExtensionType
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    # Enable Python to collect the space used by the DOM.
    doc = None
    if not silence:
        sys.stdout.write('#from ??? import *\n\n')
        sys.stdout.write('import ??? as model_\n\n')
        sys.stdout.write('rootObj = model_.rootClass(\n')
        rootObj.exportLiteral(sys.stdout, 0, name_=rootTag)
        sys.stdout.write(')\n')
    return rootObj


USAGE_TEXT = """
Usage: python ???.py <infilename>
"""


def usage():
    print(USAGE_TEXT)
    sys.exit(1)


def main():
    args = sys.argv[1:]
    if len(args) != 1:
        usage()
    infilename = args[0]
    parse(infilename)


if __name__ == '__main__':
    #import pdb; pdb.set_trace()
    main()
